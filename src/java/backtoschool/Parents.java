/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import backtoschool.service.ParentsFacadeREST;
import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "parents")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parents.findAll", query = "SELECT p FROM Parents p")
    , @NamedQuery(name = "Parents.findByCodFiscale", query = "SELECT p FROM Parents p WHERE p.codFiscale = :codFiscale")
    , @NamedQuery(name = "Parents.findByName", query = "SELECT p FROM Parents p WHERE p.name = :name")
    , @NamedQuery(name = "Parents.findBySurname", query = "SELECT p FROM Parents p WHERE p.surname = :surname")
    , @NamedQuery(name = "Parents.findByEmail", query = "SELECT p FROM Parents p WHERE p.email = :email")
    , @NamedQuery(name = "Parents.findByPassword", query = "SELECT p FROM Parents p WHERE p.password = :password")})
public class Parents extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "cod_fiscale")
    @Expose
    private String codFiscale;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    @Expose
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "surname")
    @Expose
    private String surname;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "email")
    @Expose
    private String email;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "password")
    @Expose(serialize = false, deserialize = true)
    private String password;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codParent")
    @XmlTransient
    private Collection<Appointments> appointmentsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codParent")
    @XmlTransient
    private Collection<Payments> paymentsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codFiscParent")
    @XmlTransient
    private Collection<Students> studentsCollection;
    
    @Expose
    @Transient
    private List<String> studentsChildren; //Lista codici fiscali
    



    public Parents() {
    }

    public Parents(String codFiscale) {
        super();
        this.codFiscale = codFiscale;
        //studentsChildren=new ArrayList<>();
        
    }

    public Parents(String codFiscale, String name, String surname, String email, String password) {
        super();
        this.codFiscale = codFiscale;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        //studentsChildren=new ArrayList<>();
        
    }
   
    public Parents(Parents p2){
        super();
        this.codFiscale=p2.getCodFiscale();
        this.name=p2.getName();
        this.surname=p2.getSurname();
        this.email=p2.getEmail();
        this.studentsChildren=new ArrayList<>();
        p2.getStudentsCollection().forEach(s->this.getStudentsChildren().add(s.getCodFiscale()));
        
    }
    
    public void copy(Parents p2){
        this.codFiscale=p2.getCodFiscale();
        this.name=p2.getName();
        this.surname=p2.getSurname();
        this.email=p2.getEmail();
        this.password=p2.getPassword();
        this.studentsChildren=new ArrayList<>();
        links.addAll(p2.links);
        this.getStudentsCollection().forEach(s->this.getStudentsChildren().add(s.getCodFiscale()));
        
    }

    public String getCodFiscale() {
        return codFiscale;
    }

    public void setCodFiscale(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }
    

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public Collection<Appointments> getAppointmentsCollection() {
        return appointmentsCollection;
    }

    public void setAppointmentsCollection(Collection<Appointments> appointmentsCollection) {
        this.appointmentsCollection = appointmentsCollection;
    }

    @XmlTransient
    public Collection<Payments> getPaymentsCollection() {
        return paymentsCollection;
    }

    public void setPaymentsCollection(Collection<Payments> paymentsCollection) {
        this.paymentsCollection = paymentsCollection;
    }

    @XmlTransient
    public Collection<Students> getStudentsCollection() {
        return studentsCollection;
    }

    public void setStudentsCollection(Collection<Students> studentsCollection) {
        this.studentsCollection = studentsCollection;
    }

    public List<String> getStudentsChildren() {
        return studentsChildren;
    }

    public void setStudentsChildren(List<String> studentsChildren) {
        this.studentsChildren = studentsChildren;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFiscale != null ? codFiscale.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parents)) {
            return false;
        }
        Parents other = (Parents) object;
        if ((this.codFiscale == null && other.codFiscale != null) || (this.codFiscale != null && !this.codFiscale.equals(other.codFiscale))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Parents{" + "codFiscale=" + codFiscale + ", name=" + name + ", surname=" + surname + ", email=" + email + ", studentsCollection=" + studentsCollection + '}';
    }

   
    
}
