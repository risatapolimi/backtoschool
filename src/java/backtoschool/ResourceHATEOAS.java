/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marco Sartini
 */
public class ResourceHATEOAS {
    
    @Expose(serialize = true, deserialize = false)
    @Transient
    protected List<LinkH> links;

    public ResourceHATEOAS() {
        this.links = new ArrayList<>();
    }
    
    
        
    @XmlTransient
    public LinkH getSelf() {
        return links.get(0);
    }
    
   
    public void setSelf(LinkH linkSelf){
        links.add(0,linkSelf);
    }
    
    
    public void addLink(LinkH link){
        if(links.size()>0){
            this.links.add(link);
        }
        else{
            this.links.add(1,link);
        }
    }
    
    @XmlElementWrapper(name="links")
    @XmlElement(name="link")
    public List<LinkH> getLinks(){
        return this.links;
    }
    
    public LinkH getLink(int i){
        return this.links.get(i);
    }
    
    public void clearLinks(){
        this.links.clear();   
    }
}
