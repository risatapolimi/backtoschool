/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniele
 */
@XmlRootElement
public class StudentPOJO implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String name;
    private String surname;
    private Date date;
    private String class1;
    private String codFiscale;
    private String codFiscParent;

    public StudentPOJO(Students s) {
        this.name=s.getName();
        this.surname=s.getSurname();
        this.class1=s.getClass1();
        this.date=s.getDate();
        this.codFiscale=s.getCodFiscale();
        this.codFiscParent=s.getCodFiscParent().getCodFiscale();
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getCodFiscale() {
        return codFiscale;
    }

    public void setCodFiscale(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    public String getCodFiscParent() {
        return codFiscParent;
    }

    public void setCodFiscParent(String codFiscParent) {
        this.codFiscParent = codFiscParent;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.codFiscale);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StudentPOJO other = (StudentPOJO) obj;
        if (!Objects.equals(this.codFiscale, other.codFiscale)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "StudentPOJO{" + "name=" + name + ", surname=" + surname + ", date=" + date + ", class1=" + class1 + ", codFiscale=" + codFiscale + ", codFiscParent=" + codFiscParent + '}';
    }
    
    
    public static List<StudentPOJO> convertList(List<Students> list){
        ArrayList<StudentPOJO> pojoList=new ArrayList<>();
        list.forEach(s->pojoList.add(new StudentPOJO(s)));
        return pojoList;
    }
    
}
