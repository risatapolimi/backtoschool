/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import backtoschool.service.AbstractFacade;
import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "teachers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Teachers.findAll", query = "SELECT t FROM Teachers t")
    , @NamedQuery(name = "Teachers.findByName", query = "SELECT t FROM Teachers t WHERE t.name = :name")
    , @NamedQuery(name = "Teachers.findBySurname", query = "SELECT t FROM Teachers t WHERE t.surname = :surname")
    , @NamedQuery(name = "Teachers.findByCodFiscale", query = "SELECT t FROM Teachers t WHERE t.codFiscale = :codFiscale")
    , @NamedQuery(name = "Teachers.findByEmail", query = "SELECT t FROM Teachers t WHERE t.email = :email")
    , @NamedQuery(name = "Teachers.findByPassword", query = "SELECT t FROM Teachers t WHERE t.password = :password")})
public class Teachers extends ResourceHATEOAS implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTeacher")
    @XmlTransient
    private Collection<Teaching> teachingCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTeacher")
    @XmlTransient
    private Collection<LessonTimetable> lessonTimetableCollection;

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    @Expose
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "surname")
    @Expose
    private String surname;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "cod_fiscale")
    @Expose
    private String codFiscale;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "email")
    @Expose
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "password")
    @Expose(serialize = false, deserialize = true)
    private String password; 
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTeacher")
    @XmlTransient
    private Collection<Appointments> appointmentsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTeacher")
    @XmlTransient
    private Collection<Grades> gradesCollection;
    
    public Teachers() {
    }

    public Teachers(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    public Teachers(String codFiscale, String name, String surname, String email, String password) {
        super();
        this.codFiscale = codFiscale;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }
    
    public Teachers(Teachers t2){
        super();
        this.codFiscale=t2.getCodFiscale();
        this.name=t2.getName();
        this.surname=t2.getSurname();
        this.email=t2.getEmail();        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCodFiscale() {
        return codFiscale;
    }

    public void setCodFiscale(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public Collection<Appointments> getAppointmentsCollection() {
        return appointmentsCollection;
    }

    public void setAppointmentsCollection(Collection<Appointments> appointmentsCollection) {
        this.appointmentsCollection = appointmentsCollection;
    }

    @XmlTransient
    public Collection<Grades> getGradesCollection() {
        return gradesCollection;
    }

    public void setGradesCollection(Collection<Grades> gradesCollection) {
        this.gradesCollection = gradesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFiscale != null ? codFiscale.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teachers)) {
            return false;
        }
        Teachers other = (Teachers) object;
        if ((this.codFiscale == null && other.codFiscale != null) || (this.codFiscale != null && !this.codFiscale.equals(other.codFiscale))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Teachers[ codFiscale=" + codFiscale + " ]";
    }

 
    @XmlTransient
    public Collection<LessonTimetable> getLessonTimetableCollection() {
        return lessonTimetableCollection;
    }

    public void setLessonTimetableCollection(Collection<LessonTimetable> lessonTimetableCollection) {
        this.lessonTimetableCollection = lessonTimetableCollection;
    }

    @XmlTransient
    public Collection<Teaching> getTeachingCollection() {
        return teachingCollection;
    }

    public void setTeachingCollection(Collection<Teaching> teachingCollection) {
        this.teachingCollection = teachingCollection;
    }

    public void copy(Teachers t2) {
        this.codFiscale=t2.getCodFiscale();
        this.name=t2.getName();
        this.surname=t2.getSurname();
        this.email=t2.getEmail();
        this.password=t2.getPassword();
    }
    
}
