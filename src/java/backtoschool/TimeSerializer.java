/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 *
 * @author Marco Sartini
 */
public class TimeSerializer implements JsonSerializer<Date>,JsonDeserializer<Date> {

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("HH:mm");
    public static final Locale LOCALE_HUNGARIAN = new Locale("it", "IT");
    public static final TimeZone LOCAL_TIME_ZONE = TimeZone.getTimeZone("Europe/Rome");

    @Override
    public JsonElement serialize(Date t, Type type, JsonSerializationContext gen) {
        if (t == null) {
            return gen.serialize(null);
        } else {
            return gen.serialize(FORMATTER.format(t.getTime()));
        }
    }

        @Override
	public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		String date = element.getAsString();
		
		SimpleDateFormat formatter = new SimpleDateFormat("hh:mm");
		formatter.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}
} 

