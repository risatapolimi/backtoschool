/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "lesson_timetable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LessonTimetable.findAll", query = "SELECT l FROM LessonTimetable l")
    , @NamedQuery(name = "LessonTimetable.findById", query = "SELECT l FROM LessonTimetable l WHERE l.id = :id")
    , @NamedQuery(name = "LessonTimetable.findByClass1", query = "SELECT l FROM LessonTimetable l WHERE l.class1 = :class1")
    , @NamedQuery(name = "LessonTimetable.findByArgument", query = "SELECT l FROM LessonTimetable l WHERE l.argument = :argument")
    , @NamedQuery(name = "LessonTimetable.findByTimeStart", query = "SELECT l FROM LessonTimetable l WHERE l.timeStart = :timeStart")
    , @NamedQuery(name = "LessonTimetable.findByTimeEnd", query = "SELECT l FROM LessonTimetable l WHERE l.timeEnd = :timeEnd")
    , @NamedQuery(name = "LessonTimetable.findByDay", query = "SELECT l FROM LessonTimetable l WHERE l.day = :day")})
public class LessonTimetable extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "class")
    @Expose
    private String class1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Expose
    @Column(name = "argument")
    private String argument;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_start")
    @Expose
    @Temporal(TemporalType.TIME)
    @JsonAdapter(value = TimeSerializer.class)
    private Date timeStart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_end")
    @Expose
    @Temporal(TemporalType.TIME)
    @JsonAdapter(value = TimeSerializer.class)
    private Date timeEnd;
    @Size(max = 9)
    @Column(name = "day")
    @Expose
    private String day;
    @XmlTransient
    @JoinColumn(name = "cod_teacher", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    private Teachers codTeacher;
    
    @Expose
    @Transient
    private String stringCodTeacher;

    public LessonTimetable() {
        super();
    }

    public LessonTimetable(Integer id) {
        super();
        this.id = id;
    }

    public LessonTimetable(Integer id, String class1, String argument, Date timeStart, Date timeEnd) {
        super();
        this.id = id;
        this.class1 = class1;
        this.argument = argument;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }
    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }
    
    @XmlJavaTypeAdapter(TimeAdapter.class)
    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }
    @XmlJavaTypeAdapter(TimeAdapter.class)
    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
    @XmlTransient
    public Teachers getCodTeacher() {
        return codTeacher;
    }

    public void setCodTeacher(Teachers codTeacher) {
        this.codTeacher = codTeacher;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LessonTimetable)) {
            return false;
        }
        LessonTimetable other = (LessonTimetable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.LessonTimetable[ id=" + id + " ]";
    }

    public String getStringCodTeacher() {
        return stringCodTeacher;
    }

    public void setStringCodTeacher(String stringCodTeacher) {
        this.stringCodTeacher = stringCodTeacher;
    }
    
}
