/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "appointments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appointments.findAll", query = "SELECT a FROM Appointments a")
    , @NamedQuery(name = "Appointments.findById", query = "SELECT a FROM Appointments a WHERE a.id = :id")
    , @NamedQuery(name = "Appointments.findByDate", query = "SELECT a FROM Appointments a WHERE a.date = :date")
    , @NamedQuery(name = "Appointments.findByAccepted", query = "SELECT a FROM Appointments a WHERE a.accepted = :accepted")
    , @NamedQuery(name = "Appointments.findByLastEditor", query = "SELECT a FROM Appointments a WHERE a.lastEditor = :lastEditor")
    , @NamedQuery(name = "Appointments.findByParentCalendar", query = "SELECT a FROM Appointments a WHERE a.codParent.codFiscale = :codFiscale AND a.date >= :mydate ORDER BY a.date")
    , @NamedQuery(name = "Appointments.findByTeacherCalendar", query = "SELECT a FROM Appointments a WHERE a.codTeacher.codFiscale = :codFiscale AND a.date >= :mydate ORDER BY a.date")
    , @NamedQuery(name = "Appointments.findByNote", query = "SELECT a FROM Appointments a WHERE a.note = :note")})
public class Appointments extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "accepted")
    @Expose
    private boolean accepted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_editor")
    @XmlTransient
    private boolean lastEditor; //false ->genitore, true --> insegnante
    @Size(max = 100)
    @Column(name = "note")
    @Expose
    private String note;
    @JoinColumn(name = "cod_teacher", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Teachers codTeacher;
    @JoinColumn(name = "cod_parent", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Parents codParent;
    
    
    @Expose
    @Transient
    private String stringCodTeacher;
    
    @Expose
    @Transient
    private String stringCodParent;
    

    public Appointments() {
    }

    public Appointments(Integer id) {
        this.id = id;
    }

    public Appointments(Integer id, Date date, boolean accepted, boolean lastEditor) {
        this.id = id;
        this.date = date;
        this.accepted = accepted;
        this.lastEditor = lastEditor;
    }
    
    public Appointments(Appointments a){
        this.id = a.getId();
        this.accepted=a.getAccepted();
        this.date=a.getDate();
        this.note=a.getNote();
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @XmlTransient
    public boolean getLastEditor() {
        return lastEditor;
    }

    public void setLastEditor(boolean lastEditor) {
        this.lastEditor = lastEditor;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @XmlTransient
    public Teachers getCodTeacher() {
        return codTeacher;
    }

    public void setCodTeacher(Teachers codTeacher) {
        this.codTeacher = codTeacher;
    }

    @XmlTransient
    public Parents getCodParent() {
        return codParent;
    }

    public void setCodParent(Parents codParent) {
        this.codParent = codParent;
    }

    public String getStringCodTeacher() {
        return stringCodTeacher;
    }

    public void setStringCodTeacher(String stringCodTeacher) {
        this.stringCodTeacher = stringCodTeacher;
    }

    public String getStringCodParent() {
        return stringCodParent;
    }

    public void setStringCodParent(String stringCodParent) {
        this.stringCodParent = stringCodParent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointments)) {
            return false;
        }
        Appointments other = (Appointments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Appointments[ id=" + id + " ]";
    }
    
}
