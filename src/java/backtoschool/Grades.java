/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "grades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grades.findAll", query = "SELECT g FROM Grades g")
    , @NamedQuery(name = "Grades.findByDate", query = "SELECT g FROM Grades g WHERE g.date = :date")
    , @NamedQuery(name = "Grades.findByMark", query = "SELECT g FROM Grades g WHERE g.mark = :mark")
    , @NamedQuery(name = "Grades.findById", query = "SELECT g FROM Grades g WHERE g.id = :id")
    , @NamedQuery(name = "Grades.findByStudent", query = "SELECT g FROM Grades g WHERE g.codStudent.codFiscale = :stringCodeStudent ORDER BY g.argument")
    , @NamedQuery(name = "Grades.findByStudentAndArgument", query = "SELECT g FROM Grades g WHERE g.argument = :argument AND g.codStudent.codFiscale = :stringCodeStudent")
    , @NamedQuery(name = "Grades.findByStudentAndTeacher", query = "SELECT g FROM Grades g WHERE g.codTeacher.codFiscale = :codFiscale AND g.codStudent.codFiscale = :stringCodeStudent ORDER BY g.argument")
    , @NamedQuery(name = "Grades.findByArgument", query = "SELECT g FROM Grades g WHERE g.argument = :argument")})
public class Grades extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mark")
    @Expose
    private double mark;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "argument")
    @Expose
    private String argument;
    @JoinColumn(name = "cod_student", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Students codStudent;
    @JoinColumn(name = "cod_teacher", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Teachers codTeacher;
    
    @Transient
    @Expose
    private String stringCodeStudent;
    
    @Transient
    @Expose
    private String stringCodeTeacher;

    public Grades() {
    }

    public Grades(Integer id) {
        this.id = id;
    }

    public Grades(Integer id, Date date, double mark, String argument) {
        this.id = id;
        this.date = date;
        this.mark = mark;
        this.argument = argument;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStringCodeStudent() {
        return stringCodeStudent;
    }

    public void setStringCodeStudent(String stringCodeStudent) {
        this.stringCodeStudent = stringCodeStudent;
    }
    
    

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @XmlTransient
    public Students getCodStudent() {
        return codStudent;
    }

    public void setCodStudent(Students codStudent) {
        this.codStudent = codStudent;
    }

    @XmlTransient
    public Teachers getCodTeacher() {
        return codTeacher;
    }

    public void setCodTeacher(Teachers codTeacher) {
        this.codTeacher = codTeacher;
    }

    public String getStringCodeTeacher() {
        return stringCodeTeacher;
    }

    public void setStringCodeTeacher(String stringCodeTeacher) {
        this.stringCodeTeacher = stringCodeTeacher;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grades)) {
            return false;
        }
        Grades other = (Grades) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Grades[ id=" + id + " ]";
    }
    
}
