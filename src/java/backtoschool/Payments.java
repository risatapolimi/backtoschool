/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.enterprise.inject.Default;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "payments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Payments.findAll", query = "SELECT p FROM Payments p")
    , @NamedQuery(name = "Payments.findById", query = "SELECT p FROM Payments p WHERE p.id = :id")
    , @NamedQuery(name = "Payments.findByCausal", query = "SELECT p FROM Payments p WHERE p.causal = :causal")
    , @NamedQuery(name = "Payments.findByIssuingDate", query = "SELECT p FROM Payments p WHERE p.issuingDate = :issuingDate")
    , @NamedQuery(name = "Payments.findByPaymentDate", query = "SELECT p FROM Payments p WHERE p.paymentDate = :paymentDate")
    , @NamedQuery(name = "Payments.findByCodStudent", query = "SELECT p FROM Payments p WHERE p.codStudent.codFiscale = :codFiscale")
    , @NamedQuery(name = "Payments.findByCodParent", query = "SELECT p FROM Payments p WHERE p.codParent.codFiscale = :codFiscale")
    , @NamedQuery(name = "Payments.findByCodParentAndSatus", query = "SELECT p FROM Payments p WHERE p.codParent.codFiscale = :codFiscale AND p.isPayed = :isPayed")
    , @NamedQuery(name = "Payments.findByIsPayed", query = "SELECT p FROM Payments p WHERE p.isPayed = :isPayed")
    , @NamedQuery(name = "Payments.findByAmount", query = "SELECT p FROM Payments p WHERE p.amount = :amount")})
public class Payments extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "causal")
    @Expose
    private String causal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "issuing_date")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date issuingDate;
    @Column(name = "payment_date",columnDefinition = "default null")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date paymentDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isPayed")
    @Expose
    private boolean isPayed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    @Expose
    private double amount;
    @JoinColumn(name = "cod_parent", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Parents codParent;
    @JoinColumn(name = "cod_student", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Students codStudent;
    
    @Expose
    @Transient
    private String stringCodParent;
    @Expose
    @Transient
    private String stringCodStudent;

    public Payments() {
    }

    public Payments(Integer id) {
        this.id = id;
    }

    public Payments(Integer id, String causal, Date issuingDate, boolean isPayed, double amount) {
        this.id = id;
        this.causal = causal;
        this.issuingDate = issuingDate;
        this.isPayed = isPayed;
        this.amount = amount;
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCausal() {
        return causal;
    }

    public void setCausal(String causal) {
        this.causal = causal;
    }

    public Date getIssuingDate() {
        return issuingDate;
    }

    public void setIssuingDate(Date issuingDate) {
        this.issuingDate = issuingDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public boolean getIsPayed() {
        return isPayed;
    }

    public void setIsPayed(boolean isPayed) {
        this.isPayed = isPayed;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @XmlTransient
    public Parents getCodParent() {
        return codParent;
    }

    public void setCodParent(Parents codParent) {
        this.codParent = codParent;
    }

    @XmlTransient
    public Students getCodStudent() {
        return codStudent;
    }

    public void setCodStudent(Students codStudent) {
        this.codStudent = codStudent;
    }

    public String getStringCodParent() {
        return stringCodParent;
    }

    public void setStringCodParent(String stringCodParent) {
        this.stringCodParent = stringCodParent;
    }

    public String getStringCodStudent() {
        return stringCodStudent;
    }

    public void setStringCodStudent(String stringCodStudent) {
        this.stringCodStudent = stringCodStudent;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Payments)) {
            return false;
        }
        Payments other = (Payments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Payments[ id=" + id + " ]";
    }
    
}
