/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "token")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Token.findAll", query = "SELECT t FROM Token t")
    , @NamedQuery(name = "Token.findByCodUser", query = "SELECT t FROM Token t WHERE t.codUser = :codUser")
    , @NamedQuery(name = "Token.findByValue", query = "SELECT t FROM Token t WHERE t.value = :value")
    , @NamedQuery(name = "Token.findByTimestamp", query = "SELECT t FROM Token t WHERE t.timestamp = :timestamp")
    , @NamedQuery(name = "Token.findByUserType", query = "SELECT t FROM Token t WHERE t.userType = :userType")})
public class Token extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "cod_user")
    @Expose
    private String codUser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "value")
    @Expose
    private String value;
    @Basic(optional = false)
    @NotNull
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    @Expose
    private Date timestamp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "user_type")
    @Expose
    private String userType;

    public Token() {
    }

    public Token(String codUser) {
        this.codUser = codUser;
    }

    public Token(String codUser, String emailUser, String pwdUser, String userType) {
        this.codUser = codUser;
        this.userType = userType;
        this.createTokenValue(emailUser, pwdUser);
    }

    public String getCodUser() {
        return codUser;
    }

    public void setCodUser(String codUser) {
        this.codUser = codUser;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codUser != null ? codUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Token)) {
            return false;
        }
        Token other = (Token) object;
        if ((this.codUser == null && other.codUser != null) || (this.codUser != null && !this.codUser.equals(other.codUser))) {
            return false;
        }
        return true;
    }
    
     private void createTokenValue(String emailUser, String pwdUser){
        GregorianCalendar cal=new GregorianCalendar();
        Date now=cal.getTime();
        String val=""+cal.get(Calendar.DAY_OF_YEAR)+cal.get(Calendar.SECOND)+this.codUser+cal.get(Calendar.MINUTE)+cal.get(Calendar.YEAR)+cal.get(Calendar.HOUR_OF_DAY);
        byte[] encodedBytes = Base64.getEncoder().encode((emailUser+":"+pwdUser).getBytes());
        this.value = new String(encodedBytes);
        this.timestamp=new Date();
        
     }

    @Override
    public String toString() {
        return "Token [ codUser=" + codUser + ", value="+value+", timestamp="+timestamp+", user_typw="+userType+" ]";
    }
    
}
