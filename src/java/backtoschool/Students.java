/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "students")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Students.findAll", query = "SELECT s FROM Students s GROUP BY s.class1 ORDER BY s.class1")
    , @NamedQuery(name = "Students.findByName", query = "SELECT s FROM Students s WHERE s.name = :name")
    , @NamedQuery(name = "Students.findBySurname", query = "SELECT s FROM Students s WHERE s.surname = :surname")
    , @NamedQuery(name = "Students.findByDate", query = "SELECT s FROM Students s WHERE s.date = :date")
    , @NamedQuery(name = "Students.findByClass1", query = "SELECT s FROM Students s WHERE s.class1 = :class1")
    , @NamedQuery(name = "Students.findByCodFiscale", query = "SELECT s FROM Students s WHERE s.codFiscale = :codFiscale")
    , @NamedQuery(name = "Students.findChildrenClass", query = "SELECT DISTINCT(s.class1) FROM Students s WHERE s.codFiscParent.codFiscale = :codFiscale")
    , @NamedQuery(name = "Students.findByCodParent", query = "SELECT s FROM Students s WHERE s.codFiscParent.codFiscale = :codFiscale")})
public class Students extends ResourceHATEOAS implements Serializable {

    @XmlTransient
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codStudent")
    private Collection<Payments> paymentsCollection;

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    @Expose
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "surname")
    @Expose
    private String surname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date date;
    @Size(max = 10)
    @Column(name = "class")
    @Expose
    private String class1;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "cod_fiscale")
    @Expose
    private String codFiscale;
    @JoinColumn(name = "cod_fisc_parent", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Parents codFiscParent;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codStudent")
    @XmlTransient
    private Collection<Grades> gradesCollection;
   
    
    @Transient
    @Expose
    private String stringCodeParent;

    public Students() {
        super();
    }

    public Students(String codFiscale) {
        super();
        this.codFiscale = codFiscale;
    }

    public Students(String codFiscale, String name, String surname, Date date) {
        super();
        this.codFiscale = codFiscale;
        this.name = name;
        this.surname = surname;
        this.date = date;
    }
    
       public Students(String codFiscale, String name, String surname, Date date, String class1, String cfParent) {
        super();
        this.codFiscale = codFiscale;
        this.name = name;
        this.surname = surname;
        this.date = date;
        this.class1 = class1;
        this.stringCodeParent = cfParent;
    }
    
    public void copy(Students s2){
        this.name=s2.getName();
        this.surname=s2.getSurname();
        this.date=s2.getDate();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getCodFiscale() {
        return codFiscale;
    }

    public void setCodFiscale(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    @XmlTransient
    public Parents getCodFiscParent() {
        return codFiscParent;
    }

    public void setCodFiscParent(Parents codFiscParent) {
        this.codFiscParent = codFiscParent;
    }

    @XmlTransient
    public Collection<Grades> getGradesCollection() {
        return gradesCollection;
    }

    public String getStringCodeParent() {
        return stringCodeParent;
    }

    public void setStringCodeParent(String stringCodeParent) {
        this.stringCodeParent = stringCodeParent;
    }

    public void setGradesCollection(Collection<Grades> gradesCollection) {
        this.gradesCollection = gradesCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codFiscale != null ? codFiscale.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Students)) {
            return false;
        }
        Students other = (Students) object;
        if ((this.codFiscale == null && other.codFiscale != null) || (this.codFiscale != null && !this.codFiscale.equals(other.codFiscale))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Students[ codFiscale=" + codFiscale + " ]";
    }

    @XmlTransient
    public Collection<Payments> getPaymentsCollection() {
        return paymentsCollection;
    }

    public void setPaymentsCollection(Collection<Payments> paymentsCollection) {
        this.paymentsCollection = paymentsCollection;
    }
    
}
