/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "teaching")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Teaching.findAll", query = "SELECT t FROM Teaching t")
    , @NamedQuery(name = "Teaching.findByClass1", query = "SELECT t FROM Teaching t WHERE t.class1 = :class1")
    , @NamedQuery(name = "Teaching.findByArgument", query = "SELECT t FROM Teaching t WHERE t.argument = :argument")
    , @NamedQuery(name = "Teaching.findByCodTeacher", query = "SELECT t FROM Teaching t WHERE t.codTeacher.codFiscale = :codTeacher")
    , @NamedQuery(name = "Teaching.findByClassAndCodTeacher", query = "SELECT t FROM Teaching t WHERE t.codTeacher.codFiscale = :codTeacher AND t.class1 = :class1")
    , @NamedQuery(name = "Teaching.findClassByCodTeacher", query = "SELECT DISTINCT(t.class1) FROM Teaching t WHERE t.codTeacher.codFiscale = :codTeacher")
    , @NamedQuery(name = "Teaching.findByClassAndArgument", query = "SELECT t FROM Teaching t WHERE t.codTeacher.codFiscale = :codTeacher AND t.class1 = :class1 AND t.argument = :argument")
    , @NamedQuery(name = "Teaching.findTeachersByClass1", query = "SELECT DISTINCT (t.codTeacher) FROM Teaching t WHERE t.class1 = :class1")
        , @NamedQuery(name = "Teaching.findById", query = "SELECT t FROM Teaching t WHERE t.id = :id")})

public class Teaching extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "class")
    @Expose
    private String class1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "argument")
    @Expose
    private String argument;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @JoinColumn(name = "cod_teacher", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Teachers codTeacher;
    
    @Expose
    @Transient
    private String stringCodeTeacher;

    public Teaching() {
    }

    public Teaching(Integer id) {
        this.id = id;
    }

    public Teaching(Integer id, String class1, String argument) {
        this.id = id;
        this.class1 = class1;
        this.argument = argument;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Teachers getCodTeacher() {
        return codTeacher;
    }

    public void setCodTeacher(Teachers codTeacher) {
        this.codTeacher = codTeacher;
    }

    public String getStringCodeTeacher() {
        return stringCodeTeacher;
    }

    public void setStringCodeTeacher(String stringCodeTeacher) {
        this.stringCodeTeacher = stringCodeTeacher;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teaching)) {
            return false;
        }
        Teaching other = (Teaching) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Teaching[ id=" + id + " ]";
    }
    
}
