/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.LessonTimetable;
import backtoschool.LinkH;
import backtoschool.Teachers;
import backtoschool.Teaching;
import static backtoschool.service.AbstractFacade.baseUri;
import backtoschool.Token;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("timetable")
public class LessonTimetableFacadeREST extends AbstractFacade<LessonTimetable> {
    
    @Context
    UriInfo uriInfo;
    
    Link timetableL;
    Link teachingsL, appointmentsL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    @EJB
    private TeachingFacadeREST teachingFacadeREST;
    @EJB
    private TeachersFacadeREST teacherFacade;

    public LessonTimetableFacadeREST() {
        super(LessonTimetable.class);
        localPath = "timetable/";
        appointmentsL = Link.fromUri(baseUri+"appointments").rel("appointments").title("GET").build();
        teachingsL = Link.fromUri(baseUri+"teachings").rel("teachings").title("GET").build();
        timetableL = Link.fromUri(baseUri+localPath).rel("self").title("GET").build();
        
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLesson(String lessonEntity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return addLesson(gson.fromJson(lessonEntity, LessonTimetable.class),type,tokenValue);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addLesson(LessonTimetable entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        Teachers teacher=teacherFacade.find(entity.getStringCodTeacher());
        if(teacher==null)
            return Response.status(Response.Status.NOT_FOUND).entity("Teacher not found").links(timetableL, teachingsL, appointmentsL).build();
        entity.setCodTeacher(teacher);
        this.create(entity);
        return Response.created(URI.create(baseUri+localPath+entity.getId())).links(timetableL, teachingsL, appointmentsL).build();
    }

    /*@PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, LessonTimetable entity) {
        
        super.edit(entity);
    }*/

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        LessonTimetable lesson=super.find(id);
        if(lesson!=null){
            super.remove(lesson);
            return Response.noContent().links(timetableL, teachingsL, appointmentsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Lesson timetable not found").links(timetableL, teachingsL, appointmentsL).build();
        }
    }

    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null){
            response=super.checkPermission(tokenValue, "teacher");
            if(response!=null)
                    return response;
        }
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        LessonTimetable lesson=super.find(id);
        if(lesson==null)
            return Response.status(Response.Status.NOT_FOUND).entity("Lesson not found").links(timetableL, teachingsL, appointmentsL).build();
        if(lesson.getCodTeacher().getCodFiscale().equals(token.getCodUser()) || token.getUserType().equals("admin")){
            lesson.setStringCodTeacher(lesson.getCodTeacher().getName() +" "+lesson.getCodTeacher().getSurname());
            injectLinks(lesson, token.getUserType());
            return super.convertType(type, lesson).links(timetableL, teachingsL, appointmentsL).build();
        }else{
           return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to see this lesson").links(timetableL, teachingsL, appointmentsL).build(); 
        }
    }
    
    
    
    @GET
    @Path("class/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findClassTimetable(@PathParam("id") String id, @HeaderParam("Authorization")String tokenValue, @HeaderParam("Accept") String type) {
        Response response=super.checkPermission(tokenValue, "teacher");
        if(response!=null)
            return response;
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        try{
            teachingFacadeREST.getEntityManager().createNamedQuery("Teaching.findByClassAndCodTeacher", Teaching.class)
                            .setParameter("codTeacher", token.getCodUser()).setParameter("class1", id).getResultList();
            List<LessonTimetable> lessons = findListByParameter("LessonTimetable.findByClass1", "class1", id);
           
            Map<String, List<LessonTimetable>> timetable = new HashMap<>();
            List<String> giorni;
            giorni = new ArrayList(Arrays.asList("monday", "tuesday", "wednesday", "thursday", "friday", "saturday"));
            giorni.forEach((g) -> {
                timetable.put(g, new ArrayList<>());
            });
            lessons.forEach((lt) -> {
                timetable.get(lt.getDay()).add(lt);
            });
            
            List<LessonTimetable> tutte = new ArrayList<>();
            timetable.values().forEach((l) -> {
                l.forEach((less) -> less.setStringCodTeacher(less.getCodTeacher().getName() +" "+less.getCodTeacher().getSurname()));
                l.sort((less1, less2) -> less1.getTimeStart().compareTo(less2.getTimeStart()));
                tutte.addAll(l);
            });
            injectLinks(tutte, token.getUserType());
            return convertType(type, tutte).links(timetableL, teachingsL, appointmentsL).build();
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.FORBIDDEN).links(timetableL, teachingsL, appointmentsL).build();
        }
         
    }

    /*@GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<LessonTimetable> findAll() {
        
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<LessonTimetable> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        
        return String.valueOf(super.count());
    }*/

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
        public Response.ResponseBuilder convertType(String type, List<LessonTimetable> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<LessonTimetable>> generic = new GenericEntity<List<LessonTimetable>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
        
            
    private void injectLinks(List<LessonTimetable> copy, String role){
        copy.forEach(t->t.clearLinks());    
            copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getId(), "self", "GET")));
            copy.forEach(t->t.addLink(new LinkH(baseUri+"students/class/"+t.getClass1(), "class", "GET")));  
        if("admin".equals(role)){
            copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getId(), "edit", "DELETE")));
        }
              
    }
    
    private void injectLinks(LessonTimetable p, String role){
        p.clearLinks();
        p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "GET"));
 
        if("admin".equals(role)){
            p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "DELETE"));
        }
        p.addLink(new LinkH(baseUri+"students/class/"+p.getClass1(), "class", "GET"));        
    }

    
}
