/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Grades;
import backtoschool.LinkH;
import backtoschool.Students;
import backtoschool.Teachers;
import backtoschool.Teaching;
import backtoschool.Token;
import java.net.URI;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("grades")
public class GradesFacadeREST extends AbstractFacade<Grades> {
    
    @Context
    UriInfo uriInfo; 
    
    Link gradesL, assignL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB
    private TeachingFacadeREST teachingFacadeREST;
    
    @EJB
    private TeachersFacadeREST teachersFacadeREST;
    
    @EJB
    private StudentsFacadeREST studentFacade;

    public GradesFacadeREST() {
        super(Grades.class);
        localPath = "grades/";
        gradesL = Link.fromUri(baseUri+localPath).rel("grades").title("GET").build();
        assignL = Link.fromUri(baseUri+localPath).rel("assign grade").title("POST").build();
    }
    
    @POST

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignMark(String mark,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        return this.assignMark(gson.toJson(mark,Grades.class),type,tokenValue);
    }
    
    @POST
    
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response assignMark(Grades mark,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "teacher");
        
        if(response!=null)
            return response;
        
        if(mark.getMark()<=0 || mark.getMark()>10)
            return Response.status(Response.Status.BAD_REQUEST).entity("This mark is impossible to assign").links(gradesL, assignL).build();
                    
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        

        try{
            Students student=studentFacade.find(mark.getStringCodeStudent());
            
            try{
                Teaching teaching = teachingFacadeREST.getEntityManager().createNamedQuery("Teaching.findByClassAndArgument", Teaching.class)
                        .setParameter("class1",student.getClass1()).setParameter("argument", mark.getArgument())
                        .setParameter("codTeacher", token.getCodUser()).getSingleResult();
                Teachers teacher=teachersFacadeREST.find(teaching.getCodTeacher().getCodFiscale());
                mark.setCodStudent(student);
                mark.setCodTeacher(teacher);
                
                this.create(mark);
                em.flush();
                
                return Response.created(URI.create(baseUri+localPath+mark.getId())).links(assignL).build();
            }
            catch(NullPointerException | NoResultException ex2){
                return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to provide grades for this argument and class").links(gradesL).build();
            }
        }
        catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Student selected does not exist").links(gradesL, assignL).build();
        }
        
    }

    
    @Override
    public void create(Grades entity) {
        super.create(entity);
    }

   /* @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Grades entity) {
        super.edit(entity);
    }*/

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "teacher");
        if(response!=null)
            return response;
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        Grades grade=super.find(id);
        if(grade!=null){
            if(!grade.getCodTeacher().getCodFiscale().equals(token.getCodUser()))
                return Response.status(Response.Status.FORBIDDEN).build();
            super.remove(grade);
            //return Response.ok(super.convertType(type, new StudentPOJO(student))).links(adminL).build();
            return Response.noContent().links(gradesL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Grades not found").links(gradesL).build();
        }
    }

    /*@GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Grades find(@PathParam("id") Integer id) {
        Grades grade = super.find(id);
        grade.setStringCodeStudent(grade.getCodStudent().getCodFiscale());
        return grade;
    }*/

    
    @GET
    @Path("{studentID}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findByStudent(@PathParam("studentID") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Grades> grades;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    List<Students> student = studentFacade.findListByParameter("Students.findByCodParent", "codFiscale", token.getCodUser());
                    
                    grades = findListByParameter("Grades.findByStudent", "stringCodeStudent", id);
                    grades.forEach(g->g.setStringCodeTeacher(g.getCodTeacher().getCodFiscale()));
                    injectLinks(grades);
                    return this.convertType(type, grades).links(gradesL).build();
                }catch(NullPointerException | NoResultException ex1){
                    return Response.status(Response.Status.FORBIDDEN).entity("This is not your son, please go away").links(gradesL).build();
                }
                
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                
                Teachers teacher=teachersFacadeREST.find(token.getCodUser());
                if(teacher == null)
                    return Response.status(Response.Status.NOT_FOUND).build();
                try{
                    grades = this.getEntityManager().createNamedQuery("Grades.findByStudentAndTeacher", Grades.class)
                            .setParameter("codFiscale",teacher.getCodFiscale()).setParameter("stringCodeStudent",id)
                            .getResultList();
                    if(grades != null){
                        if(grades.size()>0){
                            injectLinks(grades);
                            return this.convertType(type, grades).links(gradesL, assignL).build();
                        }
                    }
                    return Response.status(Response.Status.NO_CONTENT).links(gradesL).build();
                }catch(NullPointerException | NoResultException ex1){
                    return Response.status(Response.Status.NO_CONTENT).links(gradesL).build();
                }
                
            default:
                return super.checkPermission(tokenValue,"teacher");
        }
        
     
    }
    
    @Override
    public List<Grades> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Grades> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
     public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(gradesL).build();
    }


    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    public Response.ResponseBuilder convertType(String type, List<Grades> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Grades>> generic = new GenericEntity<List<Grades>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
        private void injectLinks(List<Grades> copy){
        copy.forEach(p->p.clearLinks());
        copy.forEach(p->p.setSelf(new LinkH(baseUri+localPath+p.getCodStudent().getCodFiscale(), "self", "GET")));
        copy.forEach(p->p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT and DELETE")));
        copy.forEach(p->p.addLink(new LinkH(baseUri+"appointments/teacher/"+p.getCodTeacher().getCodFiscale(), "appointment calendar", "GET")));
        
         
        
    }
    
    private void injectLinks(Grades p){
        p.clearLinks();
        p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "---"));
        p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT and DELETE")); 
        p.addLink(new LinkH(baseUri+localPath, "assign a grade", "POST"));
        p.addLink(new LinkH(baseUri+localPath+p.getCodStudent().getCodFiscale(), "grades of a student", "GET"));
   
    }
    

    
}
