/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.LinkH;
import backtoschool.Parents;
import backtoschool.Payments;
import backtoschool.Students;
import backtoschool.Token;
import java.net.URI;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("payments")
public class PaymentsFacadeREST extends AbstractFacade<Payments> {
    
    @Context
    UriInfo uriInfo;
    
    Link paymentsL, historyL, pendingL, adminL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB
    private StudentsFacadeREST studentsFacade;
    
    @EJB
    private ParentsFacadeREST parentsFacade;

    public PaymentsFacadeREST() {
        super(Payments.class);
        localPath = "payments/";
        paymentsL = Link.fromUri(baseUri+localPath).rel("payments").title("GET").build();
        historyL = Link.fromUri(baseUri+localPath+"history").rel("history").title("GET").build();
        pendingL = Link.fromUri(baseUri+localPath+"pending").rel("history").title("GET").build();
        adminL = Link.fromUri(baseUri+"administrators/").rel("administrators").title("GET, POST").build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPayment(String entityPayment,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return this.addPayment(gson.fromJson(entityPayment, Payments.class), type, tokenValue);
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addPayment(Payments entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            Students student=this.studentsFacade.find(entity.getStringCodStudent());
             try{
                Parents parent=this.parentsFacade.find(student.getCodFiscParent().getCodFiscale());
                if(entity.getIssuingDate()==null){
                    entity.setIssuingDate(new Date());
                }
                entity.setCodParent(parent);
                entity.setCodStudent(student);
                entity.setIsPayed(false);
                this.create(entity);
                em.flush();
                return Response.created(URI.create(baseUri+localPath+entity.getId())).links(paymentsL).build();
            }catch(NoResultException ex1){
                return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(paymentsL).build();
            }
        }catch(NoResultException ex1){
            return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(paymentsL).build();
        }
    }
    
    @Override
    public void create(Payments entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    public Response edit(@PathParam("id") Integer id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response!=null)
            return response;
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        try{
            Payments payment=super.find(id);
            if(payment==null)
                return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").links(paymentsL).build();
            if(!payment.getCodParent().getCodFiscale().equals(token.getCodUser()))
                return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to pay this issue").links(paymentsL).build();
            if(payment.getIsPayed())
                return Response.status(Response.Status.CONFLICT).entity("This payment is already done").links(paymentsL).build();
            payment.setIsPayed(true);
            payment.setPaymentDate(new Date());
            super.edit(payment);
            payment.setStringCodParent(payment.getCodParent().getCodFiscale());
            payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
            return super.convertType(type, payment).links(paymentsL).build();
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").links(paymentsL).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id, @HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            Payments payment=super.find(id);
            if(payment==null)
                return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").build();
            super.remove(payment);
            return Response.status(Response.Status.NO_CONTENT).build();
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").build();
        } 
    }

    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    Payments payment=super.find(id);
                    if(!payment.getCodParent().getCodFiscale().equals(token.getCodUser()))
                        return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to access to this payment").links(paymentsL).build();
                    payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                    payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    injectLink(payment, token.getUserType());
                    return super.convertType(type, payment).links(paymentsL).build();
                }catch(NullPointerException | NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").links(paymentsL).build();
                }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    Payments payment=super.find(id);
                    payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                    payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    injectLink(payment, token.getUserType());
                    return super.convertType(type, payment).links(adminL, paymentsL).build();
                }catch(NullPointerException | NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Payment not found").links(adminL, paymentsL).build();
                }
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    
    @GET
    @Path("student/{id}")
    public Response find(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Payments> payments;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                Students student=studentsFacade.find(id);
                if(!student.getCodFiscParent().getCodFiscale().equals(token.getCodUser()))
                    return Response.status(Response.Status.FORBIDDEN).entity("This is not your son, please go away").links(paymentsL).build();
                try{
                    payments=super.findListByParameter("Payments.findByCodStudent", "codFiscale", id);
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Payments not found").links(paymentsL, historyL, pendingL).build();
                }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    payments=super.findListByParameter("Payments.findByCodStudent", "codFiscale", id);
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(adminL, paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(adminL, paymentsL, historyL, pendingL).build();
                }
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    
    @GET
    @Path("pending")
    public Response findPending(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return this.findByStatus(type, tokenValue, false);
    }
    
    @GET
    @Path("history")
    public Response findHistory(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return this.findByStatus(type, tokenValue, true);
    }
    
    public Response findByStatus(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue,boolean status) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Payments> payments;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    payments=this.getEntityManager().createNamedQuery("Payments.findByCodParentAndSatus",Payments.class)
                              .setParameter("codFiscale", token.getCodUser()).setParameter("isPayed", status).getResultList();
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Payments not found").links(paymentsL, historyL, pendingL).build();
                }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    payments=this.findListByParameter("Payments.findByIsPayed", "isPayed", status);
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(adminL, paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(adminL, paymentsL, historyL, pendingL).build();
                }
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    
    
    @GET
    public Response find(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Payments> payments;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    payments=super.findListByParameter("Payments.findByCodParent", "codFiscale", token.getCodUser());
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Payments not found").links(paymentsL, historyL, pendingL).build();
                }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    payments=this.findAll();
                    for(Payments payment:payments){
                        payment.setStringCodParent(payment.getCodParent().getCodFiscale());
                        payment.setStringCodStudent(payment.getCodStudent().getCodFiscale());
                    }
                    injectLinks(payments, token.getUserType());
                    return this.convertType(type, payments).links(adminL, paymentsL, historyL, pendingL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(adminL, paymentsL, historyL, pendingL).build();
                }
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    
    

    @Override
    public List<Payments> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Payments> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(paymentsL).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    public Response.ResponseBuilder convertType(String type, List<Payments> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Payments>> generic = new GenericEntity<List<Payments>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Payments> copy, String role){
        copy.forEach(p->p.clearLinks());
        copy.forEach(p->p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "GET")));
        copy.forEach(p->p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT")));
        if("admin".equals(role)){
            copy.forEach(p->p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "DELETE")));
        }
        
    }
    
    private void injectLink(Payments p, String role){
        p.clearLinks();
        p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "GET"));
        p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT"));
        if("admin".equals(role)){
            p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "DELETE"));
        }
        p.addLink(new LinkH(baseUri+localPath, "payments", "GET"));
        p.addLink(new LinkH(baseUri+localPath+"history", "historical payments", "GET"));
        p.addLink(new LinkH(baseUri+localPath+"pending", "pending payments", "GET"));
        
    }
        
}