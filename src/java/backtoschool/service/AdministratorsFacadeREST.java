/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.LinkH;
import backtoschool.Token;
import static backtoschool.service.AbstractFacade.baseUri;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("administrators")
public class AdministratorsFacadeREST extends AbstractFacade<Administrators> {
    
    @Context
    UriInfo uriInfo;
    
    Link adminL, paymentsL, notificationsL, parentsL, teachersL, studentsL;
    
    String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                " <head>\n" +
                "  <title>Prova Administrator</title>\n" +
                " </head>";

    String footer = "</body></html>";
    
    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
 

    public AdministratorsFacadeREST() {
        super(Administrators.class);
        localPath = "administrators/";
        adminL = Link.fromUri(baseUri+localPath).rel("administrators").title("GET, POST").build();
        paymentsL = Link.fromUri(baseUri+"payments").rel("payments").title("GET, POST, PUT, DELETE").build();
        notificationsL = Link.fromUri(baseUri+"notifications").rel("notifications").title("GET, POST, PUT, DELETE").build();
        parentsL = Link.fromUri(baseUri+"parents/").rel("parents").title("GET, POST, PUT, DELETE").build();
        teachersL = Link.fromUri(baseUri+"teachers/").rel("teachers").title("GET, POST, PUT, DELETE").build();
        studentsL = Link.fromUri(baseUri+"students/").rel("students").title("GET, POST, PUT, DELETE").build();
    }

    @POST 
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    //@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addAmministratorFromHTML(@FormParam ("name") String name,
            @FormParam ("surname") String surname,
            @FormParam ("cf") String cf,
            @FormParam ("email") String email,
            @FormParam ("password") String password,
                    @HeaderParam("Authorization")String tokenValue){
        
        Administrators adminNew = new Administrators(cf, name, surname, new Date() ,email,password);
        return this.addAministrator(adminNew, tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_JSON)
    //@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addAmministrator(String adminEntity,@HeaderParam("Authorization")String tokenValue){
        return this.addAministrator(gson.fromJson(adminEntity, Administrators.class),tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_XML)
    //@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addAministrator(Administrators entity,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            super.findByParameter("Administrators.findByCodFiscale","codFiscale",entity.getCodFiscale());
            //Trovata codice fiscale uguale 
            //rispondo che esiste già un amministratore con quella mail
            return Response.status(Response.Status.CONFLICT).entity("Codice fiscale already in use").links(adminL).build();
        }catch(NoResultException ex1){
            try{
                super.findByParameter("Administrators.findByEmail","email",entity.getEmail());
                //Trovata email uguale 
                //rispondo che esiste già tale mail
                return Response.status(Response.Status.CONFLICT).entity("Mail already in use, please try with a different email").links(adminL).build();
            }catch(NoResultException ex2){
                //Non esiste un utente con quella mail 
                this.create(entity);
                em.flush();
                Token token = new Token(entity.getCodFiscale(), entity.getEmail(), entity.getPassword(), "admin");
                tokenFacade.create(token);
           	
                
                //Creazione nuovo amministratore con successo
                
                return Response.created(URI.create(baseUri+localPath+entity.getCodFiscale())).links(adminL).build();
            }
        }
    }
    
    
    @Override
    public void create(Administrators entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("id") String id, Administrators entity,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            Administrators admin=super.find(id);
            if(admin!=null){
                entity.setCodFiscale(admin.getCodFiscale());
                if(entity.getDate()==null)
                    entity.setDate(admin.getDate());
                if(entity.getEmail()==null || entity.getEmail().isEmpty())
                    entity.setEmail(admin.getEmail());
                if(entity.getPassword()==null || entity.getPassword().isEmpty())
                    entity.setPassword(admin.getPassword());
                if(entity.getName()==null || entity.getName().isEmpty())
                    entity.setName(admin.getName());
                if(entity.getSurname()==null || entity.getSurname().isEmpty())
                    entity.setSurname(admin.getSurname());
                if(!entity.getEmail().equals(admin.getEmail()) || !entity.getPassword().equals(admin.getPassword())){
                    Token token = new Token(admin.getCodFiscale(), entity.getEmail(), entity.getPassword(), "admin");
                    tokenFacade.edit(token);
                }
                super.edit(entity);
                return Response.ok().links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity("Administrator not found").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
            }
        }catch(NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Administrator not found").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        Administrators administrator=super.find(id);
        if(administrator!=null){
            super.remove(administrator);
            //return Response.ok(super.convertType(type, new StudentPOJO(student))).links(adminL).build();
            return Response.noContent().links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Administrator not found").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAdministratorByID(@PathParam("id") String id, @HeaderParam("Accept") String type) {
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        
        Administrators admin=super.find(id);
        if(admin!=null){
            Administrators copy = new Administrators(admin);
            injectLinks(copy);
            return super.convertType(type, copy).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Administrator not found").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllAdministrators(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            List<Administrators> listClass=super.findAll();
            List<Administrators> listCopy = new ArrayList<>();
            listClass.forEach(a->listCopy.add(new Administrators(a)));
            injectLinks(listCopy);
            return this.convertType(type, listCopy).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }catch(NoResultException ex){
            return Response.ok().entity("No Administrator is stored in the system").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }
    
    @GET
    @Produces({MediaType.TEXT_HTML})
    public Response findAllAdministratorsHTML(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        
            String body = "<body><form encode=\"application/x-www-form-urlencoded\" method=\"POST\" action=\""+baseUri+localPath+"\">\n" +
                "<table><thead><tr>\n" +
                "<th>ID (CF) </th>\n" +
                "<th> Name</th>\n" +
                "<th> Surname</th>\n" +
                "<th> Email</th>\n" +
                "<th> Password</th>\n" +    
                "<th> Actions</th>\n" +
                "</tr></thead><tbody>";
            
        try{
            List<Administrators> listClass=super.findAll();
            for(Administrators a: listClass){
                body = body + "	<tr>\n" +
                    "<td>"+a.getCodFiscale()+"</td>\n" +
                    "<td>"+a.getName()+"</td>\n" +
                    "<td>"+a.getSurname()+"</td>\n" +
                    "<td>"+a.getEmail()+"</td>\n" +
                    "<th> *****</th>\n" +
                    "<td><a href=\""+baseUri+localPath+a.getCodFiscale()+"\"> View and edit and delete </a>" +
                "</tr>";
            }
            body = body + "	<tr>\n" +
                    "<td><input name = \"cf\" ></td>\n" +
                    "<td><input name = \"name\"></td>\n" +
                    "<td><input name = \"surname\"></td>\n" +
                    "<td><input name = \"email\"></td>\n" +
                    "<td><input name = \"password\"></td>\n" +
                    "<td><input type=\"submit\" value=\"Add (POST)\" />" +
                "</tr>";
            body = body + "</tbody></table></body>";
           
            
            return Response.ok().entity(header + body + footer).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }catch(NoResultException ex){
            body = body + "</tbody></table></body>";
            return Response.ok().entity(header + body + footer).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.TEXT_HTML})
    public Response findAdministratorByIDHTML(@PathParam("id") String id,@HeaderParam("Accept") String type, @HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response != null)
            return response;
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Administrators admin=super.find(id);
        if(admin!=null){
            String body = "<body>\n" +
                "<form action=\""+baseUri+localPath+"\" method=\"PUT\"><fieldset><legend>Administrator</legend>";
            
            body = body + "<p><label for=\"cf\">Codice fiscale</label><input name = \"cf\" disabled=\"disabled\" value = \""+admin.getCodFiscale()+"\"></p>"+
                    "<p><label for=\"name\">Name</label><input name = \"name\" value = \""+admin.getName()+"\"></p>"+
                    "<p><label for=\"name\">Surname</label><input name = \"surname\" value = \""+admin.getSurname()+"\"></p>"+
                    "<p><label for=\"email\">Email</label><input name = \"email\" value = \""+admin.getEmail()+"\"></p>";
            body = body + "<p><input type=\"submit\" value=\"Put\" /></fieldset></p>"+
                "</form>"+
                   "<form action=\""+baseUri+localPath+admin.getCodFiscale()+"\" method=\"DELETE\">"+
                    "<p><input type=\"submit\" value=\"Delete\" /></p></form>"+
                "</body>";

            return Response.ok().entity(header + body + footer).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();

        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Administrator not found").links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
        }
    }

    @Override
    public List<Administrators> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Administrators> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

   @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(adminL, teachersL, parentsL, studentsL, paymentsL, notificationsL).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Administrators> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Administrators>> generic = new GenericEntity<List<Administrators>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            case "text/html":
                return Response.ok(entity, MediaType.TEXT_HTML);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Administrators> copy){
        copy.forEach(t->t.clearLinks());    
        copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET")));
        copy.forEach(t->t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE")));
               
    }
    
    private void injectLinks(Administrators t){
        t.clearLinks();
        t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET"));
        t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE"));
    }
}
