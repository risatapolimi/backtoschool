/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.LinkH;
import backtoschool.Students;
import backtoschool.Teachers;
import backtoschool.Token;
import static backtoschool.service.AbstractFacade.baseUri;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("teachers")
public class TeachersFacadeREST extends AbstractFacade<Teachers> {
    
    @Context
    public UriInfo uriInfo;
    
    Link teachersL, adminL;
  
    
    String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                " <head>\n" +
                "  <title>Teachers</title>\n" +
                " </head>";

    String footer = "</body></html>";

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;

    public TeachersFacadeREST() {
        super(Teachers.class);
        localPath = "teachers/";
        teachersL = Link.fromUri(baseUri+localPath).rel("teachers").title("GET").build();
        adminL = Link.fromUri(baseUri+"administrators/").rel("administrators").title("GET, POST").build();
    }

    @POST 
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addParentFromHTML(@FormParam ("name") String name,
            @FormParam ("surname") String surname,
            @FormParam ("cf") String cf,
            @FormParam ("email") String email,
            @FormParam ("password") String password,
                    @HeaderParam("Authorization")String tokenValue){
        
        Teachers teacherNew = new Teachers(cf, name, surname ,email,password);
        return this.addTeacher(teacherNew, tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTeacher(String teacherEntity,@HeaderParam("Authorization")String tokenValue){
        return this.addTeacher(gson.fromJson(teacherEntity, Teachers.class),tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_XML)
    public Response addTeacher(Teachers entity,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            
            super.findByParameter("Teachers.findByCodFiscale","codFiscale",entity.getCodFiscale());
            //Trovata codice fiscale uguale 
            //rispondo che esiste già un prof con quel codice
            return Response.status(Response.Status.CONFLICT).links(teachersL).entity("Codice fiscale already in use").build();
        }catch(NoResultException ex1){
            try{
                super.findByParameter("Teachers.findByEmail","email",entity.getEmail());
                //Trovata email uguale 
                //rispondo che esiste già tale mail
                return Response.status(Response.Status.CONFLICT).links(teachersL).entity("Mail already in use, please try with a different email").build();
            }catch(NoResultException ex2){
                //Non esiste un utente con quella mail 
                this.create(entity);
                em.flush();
                Token token = new Token(entity.getCodFiscale(), entity.getEmail(), entity.getPassword(), "teacher");
                tokenFacade.create(token);
                //Creazione nuovo insegnante con successo
                return Response.created(URI.create(baseUri+localPath+entity.getCodFiscale())).links(teachersL).build();
            }
        }
    }
    
    
    @Override
    public void create(Teachers entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editTeacherByID(String teacherEntity,@PathParam("id") String id, @HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        return this.editTeacherByID(gson.fromJson(teacherEntity, Teachers.class),id,type,tokenValue);
    }
    
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response editTeacherByID(Teachers entity,@PathParam("id") String id, @HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        
        Response response=super.checkPermission(tokenValue, "teacher");
        if(response!=null)
            return response;
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        if(token.getCodUser().equals(id)){
            Teachers teacher=super.find(id);
            if(teacher!=null){
                entity.setCodFiscale(teacher.getCodFiscale());
                if(entity.getEmail()==null || entity.getEmail().isEmpty())
                    entity.setEmail(teacher.getEmail());
                if(entity.getPassword()==null || entity.getPassword().isEmpty())
                    entity.setPassword(teacher.getPassword());
                if(entity.getName()==null || entity.getName().isEmpty())
                    entity.setName(teacher.getName());
                if(entity.getSurname()==null || entity.getSurname().isEmpty())
                    entity.setSurname(teacher.getSurname());
                if(!entity.getEmail().equals(teacher.getEmail()) || !entity.getPassword().equals(teacher.getPassword())){
                    token = new Token(teacher.getCodFiscale(), entity.getEmail(), entity.getPassword(), "teacher");
                    tokenFacade.edit(token);
                }
                teacher.copy(entity);
                super.edit(teacher);
               
                entity.setPassword(null);
                return super.convertType(type, entity).links(teachersL).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).links(teachersL).entity("Teacher not found").build();
            }
        }else{
            return Response.status(Response.Status.FORBIDDEN).links(teachersL).entity("You can not modify this teacher").build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        Teachers teacher=super.find(id);
        if(teacher!=null){
            super.remove(teacher);
            //return Response.ok(super.convertType(type, new StudentPOJO(student))).build();
            return Response.noContent().build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Teacher not found").links(teachersL).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findTeacherByID(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //Controllo dei permessi di visualizzare uno studente
        
        Response response;
        response=super.checkPermission(tokenValue, "teacher");
        if(response!=null){
            response=super.checkPermission(tokenValue, "admin");
            if(response!=null)
                return response;
        }
        
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Teachers teacher=super.find(id);
        Teachers t2 = new Teachers(teacher.getCodFiscale());
        t2.copy(teacher);
        t2.setPassword(null);
            injectLink(t2);
        if(t2!=null){
            
            return super.convertType(type, t2).links(teachersL).build();
        }else{
            
            return Response.status(Response.Status.NOT_FOUND).links(teachersL).entity("Teacher not found").build();
        }
    }
    
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllTeachers(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            List<Teachers> listTeachers=super.findAll();
            List<Teachers> copy=new ArrayList<>();
            listTeachers.forEach(t->copy.add(new Teachers(t)));
            injectLinks(copy);
            
            return this.convertType(type, copy).links(adminL, teachersL).build();
        }catch(NoResultException ex){
            return Response.ok().entity("No Teacher is stored in the system").links(adminL, teachersL).build();
        }
    }
    
     @GET
    @Produces({MediaType.TEXT_HTML})
    public Response findAllTeachersHTML(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        
            String body = "<body><form encode=\"application/x-www-form-urlencoded\" method=\"POST\" action=\""+uriInfo.getAbsolutePath()+"\">\n" +
                "<table><thead><tr>\n" +
                "<th>ID (CF) </th>\n" +
                "<th> Name</th>\n" +
                "<th> Surname</th>\n" +
                "<th> Email</th>\n" +
                "<th> Password</th>\n" +    
                "<th> Actions</th>\n" +
                "</tr></thead><tbody>";
            
        try{
            List<Teachers> listClass=super.findAll();
            for(Teachers t: listClass){
                body = body + "	<tr>\n" +
                    "<td>"+t.getCodFiscale()+"</td>\n" +
                    "<td>"+t.getName()+"</td>\n" +
                    "<td>"+t.getSurname()+"</td>\n" +
                    "<td>"+t.getEmail()+"</td>\n" +
                    "<th> *****</th>\n" +
                    "<td><a href=\""+uriInfo.getAbsolutePath()+t.getCodFiscale()+"\"> View and edit and delete </a>" +
                "</tr>";
            }
            body = body + "	<tr>\n" +
                    "<td><input name = \"cf\" ></td>\n" +
                    "<td><input name = \"name\"></td>\n" +
                    "<td><input name = \"surname\"></td>\n" +
                    "<td><input name = \"email\"></td>\n" +
                    "<td><input name = \"password\"></td>\n" +
                    "<td><input type=\"submit\" value=\"Add (POST)\" />" +
                "</tr>";
            body = body + "</tbody></table></body>";
           
            
            return Response.ok().entity(header + body + footer).links(teachersL).build();
        }catch(NoResultException ex){
            body = body + "</tbody></table></body>";
            return Response.ok().entity(header + body + footer).links(teachersL).build();
        }
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.TEXT_HTML})
    public Response findTeacherByIDHTML(@PathParam("id") String id,@HeaderParam("Accept") String type, @HeaderParam("Authorization")String tokenValue) {
        
        Response response=super.checkPermission(tokenValue, "teacher");
        if(response != null){
            response=super.checkPermission(tokenValue, "admin");
            if(response != null)
                return response;
        }
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Teachers teacher=super.find(id);
        if(teacher!=null){
            String body = "<body>\n" +
                "<form action=\""+uriInfo.getAbsolutePath()+"\" method=\"PUT\"><fieldset><legend>Teacher</legend>";
            
            body = body + "<p><label for=\"cf\">Codice fiscale</label><input name = \"cf\" disabled=\"disabled\" value = \""+teacher.getCodFiscale()+"\"></p>"+
                    "<p><label for=\"name\">Name</label><input name = \"name\" value = \""+teacher.getName()+"\"></p>"+
                    "<p><label for=\"name\">Surname</label><input name = \"surname\" value = \""+teacher.getSurname()+"\"></p>"+
                    "<p><label for=\"email\">Email</label><input name = \"email\" value = \""+teacher.getEmail()+"\"></p>"+
                    "<p><label for=\"password\">Password</label><input name = \"password\"></p>";
            
            
            body = body + "<p><input type=\"submit\" value=\"Put\" /></fieldset></p>"+
                "</form>"+
                   "<form action=\""+uriInfo.getAbsolutePath()+teacher.getCodFiscale()+"\" method=\"DELETE\">"+
                    "<p><input type=\"submit\" value=\"Delete\" /></p></form>"+
                "</body>";

            return Response.ok().entity(header + body + footer).links(teachersL).build();

        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Teacher not found").links(teachersL).build();
        }
    }
    
    @Override
    public List<Teachers> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Teachers> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
     public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(teachersL).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Teachers> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Teachers>> generic = new GenericEntity<List<Teachers>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Teachers> copy){
        copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET")));
        copy.forEach(t->t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE")));
        copy.forEach(t->t.addLink(new LinkH(baseUri+"teachings", "teachings and classes", "GET")));               
    }
    
    private void injectLink(Teachers t){
        t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET"));
        t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE"));
        t.addLink(new LinkH(baseUri+"teachings/", "teachings and classes", "GET"));
        t.addLink(new LinkH(baseUri+"notifications/", "notifications", "GET"));
        t.addLink(new LinkH(baseUri+"appointments/", "appointments", "GET"));        
    }
     
}
