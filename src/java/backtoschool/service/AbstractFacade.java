/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.URI;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;
    protected Gson gson;
    public static String baseUri = "http://localhost:8888/backtoschool/webresources/";
    public String localPath;

    
    @EJB
    public TokenFacadeREST tokenFacade;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
        gson=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {       
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }
    
    public T findByParameter(String query,String par,Object value) throws NoResultException{
       return this.getEntityManager().createNamedQuery(query, entityClass).setParameter(par,value).getSingleResult();
    }
    
    public List<T> findListByParameter(String query,String par,Object value) throws NoResultException{
       return this.getEntityManager().createNamedQuery(query, entityClass).setParameter(par,value).getResultList();
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public ResponseBuilder convertType(String type, T entity){
        switch(type){
            case "application/xml":
                return Response.ok(entity,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            case "text/html":
                
                return Response.ok(entity, MediaType.TEXT_HTML);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
     
    public Response checkPermission(String authString, String user_level){
        try{
            Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", authString);
            if(token.getUserType().equals(user_level)){
                return null;
            }else{
                return Response.status(Response.Status.FORBIDDEN).entity("PERMISSION DENIED, you can not do this operation").build();
            }
       }catch(Exception exPerm){ // NON RESULT EXCEPTION, LANCIA PRIMA UN'ALTRA ECCEZIONE
           return Response.status(Response.Status.UNAUTHORIZED).entity("You aren't logged, login to do stuff").header("WWW-Authenticate", "Basic").build();
       }
    }
    
    public static String splitAuthToken(String authString){
        String[] authParts = authString.split("\\s+");
        return authParts[1];
    }
    
    public static Response UnauthorizedResponse(){
        return Response.status(Response.Status.UNAUTHORIZED).entity("You are not logged in, please provide your credentials").header("WWW-Authenticate", "Basic").build();
    }
    
}
