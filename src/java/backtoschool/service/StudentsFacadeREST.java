/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.LinkH;
import backtoschool.Parents;
import backtoschool.StudentPOJO;
import backtoschool.Students;
import backtoschool.Teachers;
import backtoschool.Teaching;
import backtoschool.Token;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("students")
public class StudentsFacadeREST extends AbstractFacade<Students> {
    
    @Context
    UriInfo uriInfo;
    
    @EJB
    private TeachingFacadeREST teachingFacadeREST;  
    
    Link studentsL, timetableL, teachingsL,appointmentsL;    
    String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                " <head>\n" +
                "  <title>Prova Administrator</title>\n" +
                " </head>";

    String footer = "</body></html>";

    @EJB
    private ParentsFacadeREST parentsFacade;
    
    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;

    public StudentsFacadeREST() {
        super(Students.class);
        localPath = "students/";
        studentsL = Link.fromUri(baseUri+localPath).rel("students").title("GET").build();
        teachingsL = Link.fromUri(baseUri+"teachings/").rel("self").title("GET").build();
        appointmentsL = Link.fromUri(baseUri+"appointments/").rel("self").title("GET, POST").build();
        
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addStudentFromHTML(@FormParam ("name") String name,
            @FormParam ("surname") String surname,
            @FormParam ("cf") String cf,
            @FormParam("date") String date,
            @FormParam ("class1") String class1,
            @FormParam ("parent") String parent,
                    @HeaderParam("Authorization")String tokenValue){
   
        try{
            Date d = new SimpleDateFormat("dd/MM/yyyy").parse(date);
            Students studentNew = new Students(cf, name, surname, d , class1, parent);
            return this.addStudent(studentNew, tokenValue);
        }catch(ParseException e){
            return this.addStudent(new Students(cf, name, surname, new Date(), class1, parent), tokenValue);
        }
        catch(Exception e){
            return Response.status(Response.Status.BAD_REQUEST).entity("Check your data, please").links(studentsL).build();
        }
        
    }
    
    @POST 
    //@Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addStudent(String studentEntity,@HeaderParam("Authorization")String tokenValue){
        return this.addStudent(gson.fromJson(studentEntity, Students.class),tokenValue);
    }
    
    @POST 
    //@Path("add")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addStudent(Students entity,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            super.findByParameter("Students.findByCodFiscale","codFiscale",entity.getCodFiscale());
            //Trovata codice fiscale uguale 
            //rispondo che esiste già uno studente con quel codice
            return Response.status(Response.Status.CONFLICT).entity("Codice fiscale already in use").links(studentsL).build();
        }catch(NullPointerException | NoResultException ex1){
            try{
                //Trovo se esiste già il genitore corrispondente a quello studente
                Parents parent = parentsFacade.findByParameter("Parents.findByCodFiscale","codFiscale",entity.getStringCodeParent());
                //Esiste un genitore con quel codice fiscale
                //Posso creare lo studente
                entity.setCodFiscParent(parent);
                this.create(entity); 
                em.flush();
                //Creazione Studente nuovo con successo
                return Response.created(URI.create(baseUri+localPath+entity.getCodFiscale())).links(studentsL).build();
                //return Response.ok("Student added correctly").links(studentsL).build();
            }catch(Exception ex2){
                // Non ha trovato un parente con quel cosice fiscale, risponde con errore
                return Response.status(Response.Status.CONFLICT).entity("Parent does not exist, please insert him before").links(studentsL).build();
            }
        }
    }

    @Override
    public void create(Students entity) {
        super.create(entity);
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") String id,String studentEntity, @HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        return this.edit(id,gson.fromJson(studentEntity, Students.class),type,tokenValue);
    }
    
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response edit(@PathParam("id") String id,Students entity, @HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    Students student=super.find(id);
                    if(student==null)
                        return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
                    if(!student.getCodFiscParent().getCodFiscale().equals(token.getCodUser()))
                       return Response.status(Response.Status.FORBIDDEN).entity("This is not your son, please go away").links(studentsL).build();
                    if(entity.getName()==null || entity.getName().isEmpty())
                        entity.setName(student.getName());
                    if(entity.getSurname()==null || entity.getSurname().isEmpty())
                        entity.setSurname(student.getSurname());
                    if(entity.getDate()==null)
                        entity.setDate(student.getDate());
                    if(entity.getClass1()==null || entity.getClass1().isEmpty())
                        entity.setClass1(student.getClass1());
                    student.copy(entity);
                    super.edit(student);
                    student.setStringCodeParent(student.getCodFiscParent().getCodFiscale());
                    return super.convertType(type, student).links(studentsL).build();
                }catch(NullPointerException | NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
                }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    Students student=super.find(id);
                    student.setClass1(entity.getClass1());
                    super.edit(student);
                    student.setStringCodeParent(student.getCodFiscParent().getCodFiscale());
                    return super.convertType(type, student).links(studentsL).build();
                }catch(NoResultException ex){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
                }
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }       
    }
    

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        Students student=super.find(id);
        if(student!=null){
            super.remove(student);
            return Response.noContent().links(studentsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
        }
    }
    

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //Controllo dei permessi di visualizzare uno studente
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    Students student=super.find(id);
                    if(!student.getCodFiscParent().getCodFiscale().equals(token.getCodUser()))
                        return Response.status(Response.Status.FORBIDDEN).entity("This is not your son, plase go away").links(studentsL).build(); 
                    student.setStringCodeParent(student.getCodFiscParent().getCodFiscale());
                    injectLink(student, token.getUserType());
                    return this.convertType(type, student).links(studentsL).build();
                }catch(NoResultException ex1){
                    return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
                }             
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                try{
                    Students student=super.find(id);
                    student.setStringCodeParent(student.getCodFiscParent().getCodFiscale());
                    injectLink(student, token.getUserType());
                    return super.convertType(type, student).links(studentsL).build();
                }catch(NoResultException ex2){
                        return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
                }
            default: 
                return super.checkPermission(tokenValue, "admin");
        }
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.TEXT_HTML})
    public Response findStudentByIDHTML(@PathParam("id") String id,@HeaderParam("Accept") String type, @HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response != null){
            response=super.checkPermission(tokenValue, "admin");
            if(response != null)
                return response;
        }
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Students student=super.find(id);
        if(student!=null){
            String body = "<body>\n" +
                "<form action=\""+baseUri+localPath+"\" method=\"PUT\"><fieldset><legend>Student</legend>";
            
            body = body + "<p><label for=\"cf\">Codice fiscale</label><input name = \"cf\" disabled=\"disabled\" value = \""+student.getCodFiscale()+"\"></p>"+
                    "<p><label for=\"name\">Name</label><input name = \"name\" value = \""+student.getName()+"\"></p>"+
                    "<p><label for=\"name\">Surname</label><input name = \"surname\" value = \""+student.getSurname()+"\"></p>"+
                    "<p><label for=\"class1\">Class</label><input name = \"class1\" value = \""+student.getClass1()+"\"></p>";

            body = body + "<p><input type=\"submit\" value=\"Put\" /></fieldset></p>"+
                "</form>"+
                   "<form action=\""+baseUri+localPath+"/"+student.getCodFiscale()+"\" method=\"DELETE\">"+
                    "<p><input type=\"submit\" value=\"Delete\" /></p></form>"+
                "</body>";

            return Response.ok().entity(header + body + footer).links(studentsL).build();

        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Student not found").links(studentsL).build();
        }
    }
    
    @GET
    @Path("/{id}/teachers")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findTeacherByStudentClass(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //Controllo dei permessi di visualizzare uno studente
        
        Response response;
        response=super.checkPermission(tokenValue, "parent");
        if(response!=null){
            return response;
        }
        try{
        Students student=super.find(id);
        //List<Teachers> teachers = teachingFacadeREST.getEntityManager().createNamedQuery("Teaching.findTeachersByClass1",Teachers.class).setParameter("class", student.getClass1()).getResultList(); 
        List<Teaching> teaching = teachingFacadeREST.findListByParameter("Teaching.findByClass1", "class1", student.getClass1()); 
        
        injectLinksTeachings(teaching);
            
            return this.convertTypeTeaching(type, teaching).links().build();
        }catch(NoResultException ex){
            return Response.ok().entity("No teachers for that class is stored in the system").links().build();
        }
        
    }

    
    @GET
    @Path("class/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findStudentsByClass(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue, @PathParam("id") String classID){
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null){
            response=super.checkPermission(tokenValue, "teacher");
            if(response!=null)
                return response;
        }
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        switch (userType){
            case "teacher":
                try{
                    List<Teaching> teachings = teachingFacadeREST.getEntityManager().createNamedQuery("Teaching.findByClassAndCodTeacher", Teaching.class)
                            .setParameter("codTeacher", token.getCodUser()).setParameter("class1", classID).getResultList();
                    if(teachings != null){
                        try{
                            timetableL = Link.fromUri(baseUri+"timetable/class/"+classID).rel("timetable").title("GET").build();
                            List<Students> listClass=super.findListByParameter("Students.findByClass1","class1",classID);
                            listClass.forEach(s-> s.setStringCodeParent(s.getCodFiscParent().getCodFiscale()));
                            injectLinks(listClass, token.getUserType());
                            return this.convertType(type, listClass).links(timetableL, teachingsL, appointmentsL).build();
                        }catch(NullPointerException | NoResultException ex){
                            return Response.ok().entity("No Student is enrolled in this class").links(timetableL, teachingsL).build();
                        }
                    }
                    else{
                        Response.status(Response.Status.FORBIDDEN).links(teachingsL).build();
                    }
                    
                }catch(NoResultException e){
                    Response.status(Response.Status.FORBIDDEN).links(teachingsL).build();
                }
            case "admin":
                try{
                    List<Students> listClass=super.findListByParameter("Students.findByClass1","class1",classID);
                    listClass.forEach(s-> s.setStringCodeParent(s.getCodFiscParent().getCodFiscale()));
                    injectLinks(listClass, token.getUserType());
                    return this.convertType(type, listClass).links(studentsL).build();
                }catch(NoResultException ex){
                    return Response.ok().entity("No Student is enrolled in this class").links(studentsL).build();
                }
            default:
                return super.checkPermission(tokenValue, "admin");
        }
        
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllStudents(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //Controllo dei permessi di visualizzare uno studente
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                try{
                    List<Students> listClass=super.findListByParameter("Students.findByCodParent", "codFiscale", token.getCodUser());
                    listClass.forEach(s-> s.setStringCodeParent(s.getCodFiscParent().getCodFiscale()));
                    injectLinks(listClass, token.getUserType());
                    return this.convertType(type, listClass).links(studentsL).build();
                }catch(NoResultException ex1){
                    return Response.status(Response.Status.NOT_FOUND).entity("Students not found").links(studentsL).build();
                }             
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
            if(response!=null)
                return response;
            try{
                List<Students> listClass=super.findAll();
                listClass.forEach(s-> s.setStringCodeParent(s.getCodFiscParent().getCodFiscale()));
                injectLinks(listClass, token.getUserType());
                return this.convertType(type, listClass).links(studentsL).build();
            }catch(NoResultException ex){
                return Response.ok().entity("No Student is stored in the system").links(studentsL).build();
            }
            default: 
                return super.checkPermission(tokenValue, "admin");
        }
        
        /*Response */
    }
    
    @GET
    @Produces({MediaType.TEXT_HTML})
    public Response findAllStudentsHTML(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        
        String body = "<body><form encode=\"application/x-www-form-urlencoded\" method=\"POST\" action=\""+baseUri+localPath+"\">\n" +
                "<table><thead><tr>\n" +
                "<th>ID (CF) </th>\n" +
                "<th> Name</th>\n" +
                "<th> Surname</th>\n" +
                "<th> Class</th>\n" +   
                "<th> Parent</th>\n" + 
                "<th> Actions</th>\n" +
                "</tr></thead><tbody>";
        
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
               
                    
                  try{
                      List<Students> listClass=super.findListByParameter("Students.findByCodParent", "codFiscale", token.getCodUser());
                        for(Students s: listClass){
                            body = body + "	<tr>\n" +
                                "<td>"+s.getCodFiscale()+"</td>\n" +
                                "<td>"+s.getName()+"</td>\n" +
                                "<td>"+s.getSurname()+"</td>\n" +
                                "<td>"+s.getDate().toString()+"</td>\n" +
                                "<td>"+s.getClass1()+"</td>\n" +
                                "<td><a href=\""+baseUri+"parents/"+s.getCodFiscParent().getCodFiscale()+"\">"+ s.getCodFiscParent().getCodFiscale() +"</a></td>"+
                                "<td><a href=\""+baseUri+localPath+s.getCodFiscale()+"\"> View and edit and delete </a>" +
                            "</tr>";
                        }
                        
                        body = body + "</tbody></table></form></body>";


                        return Response.ok().entity(header + body + footer).links(studentsL).build();
                    }catch(NoResultException ex){
                        body = body + "</tbody></table></form></body>";
                        return Response.ok().entity(header + body + footer).links(studentsL).build();
                    }
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
               
                    
                    try{
                        List<Students> listClass=super.findAll();
                        for(Students s: listClass){
                            body = body + "	<tr>\n" +
                                "<td>"+s.getCodFiscale()+"</td>\n" +
                                "<td>"+s.getName()+"</td>\n" +
                                "<td>"+s.getSurname()+"</td>\n" +
                                "<td>"+s.getDate().toString()+"</td>\n" +
                                "<td>"+s.getClass1()+"</td>\n" +
                                "<td><a href=\""+baseUri+"parents/"+s.getCodFiscParent().getCodFiscale()+"\">"+ s.getCodFiscParent().getCodFiscale() +"</a></td>"+
                                "<td><a href=\""+baseUri+localPath+s.getCodFiscale()+"\"> View and edit and delete </a>" +
                            "</tr>";
                        }
                        body = body + "	<tr>\n" +
                                "<td><input name = \"cf\" ></td>\n" +
                                "<td><input name = \"name\"></td>\n" +
                                "<td><input name = \"surname\"></td>\n" +
                                "<td><input name = \"date\"></td>\n" +
                                "<td><input name = \"class1\"></td>\n" +
                                "<td><input name = \"parent\"></td>\n" +
                                "<td><input type=\"submit\" value=\"Add (POST)\" />" +
                            "</tr>";
                        body = body + "</tbody></table></form></body>";


                        return Response.ok().entity(header + body + footer).links(studentsL).build();
                    }catch(NoResultException ex){
                        body = body + "</tbody></table></form></body>";
                        return Response.ok().entity(header + body + footer).links(studentsL).build();
                    }
                
            default: 
                return super.checkPermission(tokenValue, "admin");
        }
        
        
        
    }
    
    
    
    @Override
    public List<Students> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Students> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(studentsL).build();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Students> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Students>> generic = new GenericEntity<List<Students>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    public Response.ResponseBuilder convertTypeTeaching(String type, List<Teaching> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Teaching>> generic = new GenericEntity<List<Teaching>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
        private void injectLinks(List<Students> copy, String role){
        
        copy.forEach(t->t.clearLinks());    

        copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "---")));
        if(!"teacher".equals(role)){
            copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET")));
            copy.forEach(t->t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE"))); 
        }
        else{
            copy.forEach(t->t.addLink(new LinkH(baseUri+"grades/"+t.getCodFiscale(), "grade", "GET and POST")));
        }
        copy.forEach(t->t.addLink(new LinkH(baseUri+localPath+"class/"+t.getClass1(), "students in the class", "GET")));
        
               
    }
    
    private void injectLink(Students t, String role){
        t.clearLinks();
        t.setSelf(new LinkH(baseUri+localPath+t.getCodFiscale(), "self", "GET"));
        t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale(), "edit", "PUT and DELETE"));
        t.addLink(new LinkH(baseUri+localPath+t.getCodFiscale()+"class/"+t.getClass1(), "students in the class", "GET"));  
        t.addLink(new LinkH(baseUri+"payments/student/"+t.getCodFiscale(), "payments of student", "GET"));
        if("parent".equals(role)){
            t.addLink(new LinkH(baseUri+"grades/"+t.getCodFiscale(), "grades of a student", "GET"));
        }
    }
    private void injectLinksTeachings(List<Teaching> copy){
      
        copy.forEach(t->t.clearLinks());    
        copy.forEach(t->t.setSelf(new LinkH(baseUri+"teaching/"+t.getId(), "self", "---")));
        copy.forEach(t->t.addLink(new LinkH(baseUri+"appointments/teacher/"+t.getCodTeacher().getCodFiscale(), "calendar", "GET")));
        
    }
}
