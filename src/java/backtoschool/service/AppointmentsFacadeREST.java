/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Appointments;
import backtoschool.LinkH;
import backtoschool.Parents;
import backtoschool.Payments;
import backtoschool.Teachers;
import backtoschool.Token;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("appointments")
public class AppointmentsFacadeREST extends AbstractFacade<Appointments> {
    
    @Context
    UriInfo uriInfo;
    
    Link appointmentsL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB
    private ParentsFacadeREST parentsFacade;
    
    @EJB
    private TeachersFacadeREST teacherFacade;

    public AppointmentsFacadeREST() {
        super(Appointments.class);
        localPath = "appointments/";
        appointmentsL = Link.fromUri(baseUri+localPath).rel("appointments").title("GET").build();
        
    }

    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addAppointment(String appointmentEntity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return addAppointment(gson.fromJson(appointmentEntity, Appointments.class),type,tokenValue);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addAppointment(Appointments entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        boolean lastEditor;
        Parents parent=null;
        Teachers teacher=null;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                lastEditor=false;
                parent=parentsFacade.find(token.getCodUser());    
                teacher=teacherFacade.find(entity.getStringCodTeacher());
                break;
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                lastEditor=true;
                parent=parentsFacade.find(entity.getStringCodParent());    
                teacher=teacherFacade.find(token.getCodUser());
                break;
            default:
                return super.checkPermission(tokenValue, "teacher");
        }
        if(parent==null)
           return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(appointmentsL).build();
        if(teacher==null)
            return Response.status(Response.Status.NOT_FOUND).entity("Teacher not found").links(appointmentsL).build();
        entity.setCodParent(parent);
        entity.setCodTeacher(teacher);
        entity.setLastEditor(lastEditor);
        entity.setAccepted(false);
        this.create(entity);
        em.flush();
        return Response.created(URI.create(baseUri+localPath+entity.getId())).links(appointmentsL).build();
    }
    
    
    @Override
    public void create(Appointments entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") Integer id, String entityAppointments,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        return this.edit(id, gson.fromJson(entityAppointments,Appointments.class), type, tokenValue);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response edit(@PathParam("id") Integer id, Appointments entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        Date date=new Date();
        Appointments appointment;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                appointment=super.find(id);
                //Controlli generali
                if(appointment==null)
                    return Response.status(Response.Status.NOT_FOUND).entity("Appointment not found").links(appointmentsL).build();
                if(!appointment.getCodParent().getCodFiscale().equals(token.getCodUser()))
                    return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to modify this appointment").links(appointmentsL).build();
                if(appointment.getAccepted())
                    return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity("Appointment is already accepted, you can't modify it")
                            .links(appointmentsL).header("Allow", "GET").build();
                //Controlli dati inseriti
                if(appointment.getLastEditor()==true){
                    if(entity.getAccepted()){
                       if(appointment.getDate().equals(entity.getDate()) && appointment.getNote().equals(entity.getNote())){
                           appointment.setAccepted(true);
                       }else{
                           appointment.setAccepted(false);
                       }
                    }else{
                        appointment.setAccepted(false);  
                    }
                }else{
                    appointment.setAccepted(false);
                }
                if(appointment.getAccepted()==false){
                    if(entity.getDate()!=null)
                        if(entity.getDate().before(date)){
                            return Response.status(Response.Status.FORBIDDEN).entity("You can't go back in time, you are not Doc Brown").links(appointmentsL).build();
                        }
                        else{
                            appointment.setDate(entity.getDate());
                        }
                        if(appointment.getNote()!=null){
                            appointment.setNote(entity.getNote());
                        }
                        appointment.setLastEditor(false);
                }
                this.edit(appointment);
                appointment.setStringCodParent(appointment.getCodParent().getCodFiscale());
                appointment.setStringCodTeacher(appointment.getCodTeacher().getCodFiscale());
                injectLinks(appointment, token.getUserType());
                return super.convertType(type, appointment).links(appointmentsL).build();
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                appointment=super.find(id);
                if(appointment==null)
                    return Response.status(Response.Status.NOT_FOUND).entity("Appointment not found").links(appointmentsL).build();
                if(!appointment.getCodTeacher().getCodFiscale().equals(token.getCodUser()))
                    return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to modify this appointment").links(appointmentsL).build();
                if(appointment.getAccepted()){
                    injectLinks(appointment, token.getUserType());
                    return Response.status(Response.Status.METHOD_NOT_ALLOWED).entity("Appointment is already accepted, you can't modify it")
                            .links(appointmentsL).header("Allow", "GET").build();
                }
                if(appointment.getLastEditor()==false){
                    if(entity.getAccepted()){
                       if(appointment.getDate().equals(entity.getDate()) && appointment.getNote().equals(entity.getNote())){
                           appointment.setAccepted(true);
                       }else{
                           appointment.setAccepted(false);
                       }
                    }else{
                        appointment.setAccepted(false);  
                    }
                }else{
                    appointment.setAccepted(false);
                }
                if(appointment.getAccepted()==false){
                    if(entity.getDate()!=null)
                        if(entity.getDate().before(date)){
                            return Response.status(Response.Status.FORBIDDEN).entity("You can't go back in time, you are not Doc Brown").links(appointmentsL).build();
                        }
                        else{
                            appointment.setDate(entity.getDate());
                        }
                        if(appointment.getNote()!=null){
                            appointment.setNote(entity.getNote());
                        }
                        appointment.setLastEditor(false);
                }
                this.edit(appointment);
                appointment.setStringCodParent(appointment.getCodParent().getCodFiscale());
                appointment.setStringCodTeacher(appointment.getCodTeacher().getCodFiscale());
                injectLinks(appointment, token.getUserType());
                return super.convertType(type, appointment).links(appointmentsL).build();
            default:
                return super.checkPermission(tokenValue, "teacher");
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id, @HeaderParam("Authorization")String tokenValue) {
       Response response=super.checkPermission(tokenValue, "parent");
  
        if(response!=null){
            response=super.checkPermission(tokenValue, "teacher");
            if(response!=null)
                    return response;
        }
        try{
            Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        Appointments appoint=super.find(id);
        if(appoint==null)
            return Response.status(Response.Status.NOT_FOUND).entity("Appointment not found").links(appointmentsL).build();
        if(!appoint.getCodParent().getCodFiscale().equals(token.getCodUser()) && !appoint.getCodTeacher().getCodFiscale().equals(token.getCodUser()))
            return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to see this appointement").links(appointmentsL).build();
            if(!appoint.getAccepted() || token.getUserType().equals("teacher")){
                super.remove(appoint);
                return Response.noContent().build();
            }
            else{
                return Response.status(Response.Status.METHOD_NOT_ALLOWED).build();
            }
        }
        catch(Exception e){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("{id}")
    public Response find(@PathParam("id") Integer id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "parent");
  
        if(response!=null){
            response=super.checkPermission(tokenValue, "teacher");
            if(response!=null)
                    return response;
        }
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        Appointments appoint=super.find(id);
        if(appoint==null)
            return Response.status(Response.Status.NOT_FOUND).entity("Appointment not found").links(appointmentsL).build();
        if(!appoint.getCodParent().getCodFiscale().equals(token.getCodUser()) && !appoint.getCodTeacher().getCodFiscale().equals(token.getCodUser()))
            return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to see this appointement").links(appointmentsL).build();
        appoint.setStringCodParent(appoint.getCodParent().getCodFiscale());
        appoint.setStringCodTeacher(appoint.getCodTeacher().getCodFiscale());
        injectLinks(appoint, token.getUserType());
        return super.convertType(type, appoint).links(appointmentsL).build();
    }
    
    
    @GET
    public Response find(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        Date today=new Date();
        List<Appointments> as;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                as=this.getEntityManager().createNamedQuery("Appointments.findByParentCalendar", Appointments.class)
                        .setParameter("codFiscale", token.getCodUser()).setParameter("mydate", today).getResultList();
                for(Appointments appoint:as){
                    appoint.setStringCodParent(appoint.getCodParent().getCodFiscale());
                    appoint.setStringCodTeacher(appoint.getCodTeacher().getCodFiscale());
                }
                injectLinks(as, token.getUserType());
                return this.convertType(type, as).links(appointmentsL).build();
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                as=this.getEntityManager().createNamedQuery("Appointments.findByTeacherCalendar", Appointments.class)
                        .setParameter("codFiscale", token.getCodUser()).setParameter("mydate", today).getResultList();
                for(Appointments appoint:as){
                    appoint.setStringCodParent(appoint.getCodParent().getCodFiscale());
                    appoint.setStringCodTeacher(appoint.getCodTeacher().getCodFiscale());
                }
                injectLinks(as, token.getUserType());
                return this.convertType(type, as).links(appointmentsL).build();
            default:
                return super.checkPermission(tokenValue, "teacher");           
        }
    } 
    
    @GET
    @Path("teacher/{teacherid}") 
    public Response findByTeacher(@PathParam("teacherid") String teacherid,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response!=null)
            return response;
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        Date today=new Date();
        List<Appointments> as;
        as=this.getEntityManager().createNamedQuery("Appointments.findByTeacherCalendar", Appointments.class)
                        .setParameter("codFiscale", teacherid).setParameter("mydate", today).getResultList();
        Appointments a;
        List<Appointments> copy=new ArrayList<>();
        for(Appointments appoint:as){
                    a=new Appointments(appoint);
                    a.setStringCodParent(appoint.getCodParent().getCodFiscale());
                    a.setStringCodTeacher(appoint.getCodTeacher().getCodFiscale());
                    a.setNote(null);
                    if(appoint.getAccepted())
                        copy.add(a);
        }
        injectLinksParent(copy, "parent", token.getCodUser());
        return this.convertType(type, copy).links(appointmentsL).build();
    }
    
    @Override
    public List<Appointments> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Appointments> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(appointmentsL).build();
    }
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Appointments> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Appointments>> generic = new GenericEntity<List<Appointments>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Appointments> copy, String role){
        copy.forEach(t->t.clearLinks()); 
        copy.forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getId(), "self", "GET")));
        copy.forEach(t->{
                if(!t.getAccepted() || "teacher".equals(role))
                    t.addLink(new LinkH(baseUri+localPath+t.getId(), "edit", "PUT and DELETE"));
                });
     
               
    }
    
    private void injectLinksParent(List<Appointments> copy, String role, String codFiscale){
        copy.forEach(t->t.clearLinks()); 
        copy.stream().filter(t->t.getStringCodParent().equals(codFiscale)).forEach(t->t.setSelf(new LinkH(baseUri+localPath+t.getId(), "self", "GET")));
        copy.forEach(t->{
                if(!t.getAccepted() || "teacher".equals(role))
                    t.addLink(new LinkH(baseUri+localPath+t.getId(), "edit", "PUT and DELETE"));
                });
     
               
    }
    
    private void injectLinks(Appointments t, String role){
        t.clearLinks();
        t.setSelf(new LinkH(baseUri+localPath+t.getId(), "self", "GET"));
        if(!t.getAccepted() || "teacher".equals(role)){
            t.addLink(new LinkH(baseUri+localPath+t.getId(), "edit", "PUT and DELETE"));
        }
    }
        
}
