/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.LinkH;
import backtoschool.Parents;
import backtoschool.Students;
import backtoschool.Teachers;
import backtoschool.Token;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.taglibs.standard.extra.spath.AbsolutePath;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("parents")
public class ParentsFacadeREST extends AbstractFacade<Parents> {
    
    @Context
    public UriInfo uriInfo;
    
    
    
    Link parentsL, adminL;
    Link notificationsL;
    
    String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                " <head>\n" +
                "  <title>Prova Administrator</title>\n" +
                " </head>";

    String footer = "</body></html>";
   

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB
    private StudentsFacadeREST studentFacade;

    public ParentsFacadeREST() {
        super(Parents.class);
        localPath = "parents/";
        parentsL = Link.fromUri(baseUri+localPath).rel("parents").title("GET").build();
        notificationsL = Link.fromUri(baseUri+"notifications/").rel("notifications").title("GET, PUT, DELETE").build();
        adminL = Link.fromUri(baseUri+"administrators/").rel("administrators").title("GET, POST").build();
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addParentFromHTML(@FormParam ("name") String name,
            @FormParam ("surname") String surname,
            @FormParam ("cf") String cf,
            @FormParam ("email") String email,
            @FormParam ("password") String password,
                    @HeaderParam("Authorization")String tokenValue){
        
        Parents parentNew = new Parents(cf, name, surname ,email,password);
        return this.addParent(parentNew, tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_JSON)
    //@Produces(MediaType.APPLICATION_JSON)
    public Response addParent(String parentEntity,@HeaderParam("Authorization")String tokenValue){
        return this.addParent(gson.fromJson(parentEntity, Parents.class),tokenValue);
    }
    
    @POST 
    @Consumes(MediaType.APPLICATION_XML)
    //@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addParent(Parents entity,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            super.findByParameter("Parents.findByCodFiscale","codFiscale",entity.getCodFiscale());
            //Trovata codice fiscale uguale 
            //rispondo che esiste già uno genitore con quel codice
            return Response.status(Response.Status.CONFLICT).entity("Codice fiscale already in use").links(parentsL).build();
        }catch(NoResultException ex1){
            try{
                super.findByParameter("Parents.findByEmail","email",entity.getEmail());
                //Trovata email uguale 
                //rispondo che esiste già tale mail
                return Response.status(Response.Status.CONFLICT).entity("Mail already in use, please try with a different email").links(parentsL).build();
            }catch(NoResultException ex2){
                //Non esiste un utente con quella mail 
                this.create(entity);
                em.flush();
                Token token = new Token(entity.getCodFiscale(), entity.getEmail(), entity.getPassword(), "parent");
                tokenFacade.create(token);
                //Creazione nuovo insegnante con successo
                return Response.created(URI.create(baseUri+localPath+entity.getCodFiscale())).links(parentsL).build();
            }
        }
    }
    
    
    @Override
    public void create(Parents entity) {
        super.create(entity);
    }
    
    /*"studentsChildren": [
        "mrclvl220902e512e",
        "rcrlvl170701e497r"
    ]*/
    
    
    @PUT
    @Path("{id}")
    @Consumes( MediaType.APPLICATION_JSON)
    public Response edit(@PathParam("id") String id, String entityParents,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        return this.edit(id, gson.fromJson(entityParents,Parents.class), type, tokenValue);
    }
    

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_XML)
    public Response edit(@PathParam("id") String id, Parents entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response!=null)
            return response;
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        if(token.getCodUser().equals(id)){
            Parents parent=super.find(id);
            if(parent!=null){
                entity.setCodFiscale(parent.getCodFiscale());
                if(entity.getEmail()==null || entity.getEmail().isEmpty())
                    entity.setEmail(parent.getEmail());
                if(entity.getPassword()==null || entity.getPassword().isEmpty())
                    entity.setPassword(parent.getPassword());
                if(entity.getName()==null || entity.getName().isEmpty())
                    entity.setName(parent.getName());
                if(entity.getSurname()==null || entity.getSurname().isEmpty())
                    entity.setSurname(parent.getSurname());
                if(!entity.getEmail().equals(parent.getEmail()) || !entity.getPassword().equals(parent.getPassword())){
                    token = new Token(parent.getCodFiscale(), entity.getEmail(), entity.getPassword(), "parent");
                    tokenFacade.edit(token);
                }
                parent.copy(entity);
                super.edit(parent);
                
                entity=new Parents(parent);
                return super.convertType(type, entity).links(parentsL).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(parentsL).build();
            }
        }else{
            return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to modify this parent").links(parentsL).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        Parents parent=super.find(id);
        if(parent!=null){
            super.remove(parent);
            return Response.status(Response.Status.NO_CONTENT).links(parentsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(parentsL).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") String id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response!=null)
            return response;
        
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if(!token.getCodUser().equals(id))
            return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to access to these data").links(parentsL).build();
        Parents parent=super.find(id);
        if(parent!=null){

            Parents copy=new Parents(parent);
            injectLink(copy);
            return super.convertType(type, copy).links(parentsL).build();
        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(parentsL).build();
        }
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.TEXT_HTML})
    public Response findParentByIDHTML(@PathParam("id") String id,@HeaderParam("Accept") String type, @HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "parent");
        
        if(response != null){
            response=super.checkPermission(tokenValue, "admin");
            if(response != null)
                return response;
        }
        //CONTROLLA SE LO TROVA E POI LO CONVERTE NEL FORMATO RICHIESTO 
        Parents parent=super.find(id);
        if(parent!=null){
            String body = "<body>\n" +
                "<form action=\""+uriInfo.getAbsolutePath().toASCIIString()+"\" method=\"PUT\"><fieldset><legend>Parent</legend>";
            
            body = body + "<p><label for=\"cf\">Codice fiscale</label><input name = \"cf\" disabled=\"disabled\" value = \""+parent.getCodFiscale()+"\"></p>"+
                    "<p><label for=\"name\">Name</label><input name = \"name\" value = \""+parent.getName()+"\"></p>"+
                    "<p><label for=\"name\">Surname</label><input name = \"surname\" value = \""+parent.getSurname()+"\"></p>"+
                    "<p><label for=\"email\">Email</label><input name = \"email\" value = \""+parent.getEmail()+"\"></p>"+
                    "<p><label for=\"password\">Password</label><input name = \"password\"></p>";
            
            List<Students> studentss = studentFacade.findListByParameter("Students.findByCodParent", "codFiscale", parent.getCodFiscale()); 
            
            int i=0;
            for(Students s : studentss){
                body = body + "<p><a href=\""+AbstractFacade.baseUri+"students/"+s.getCodFiscale()+"\">Figlio "+s.getCodFiscale()+"</a></p>";
            }
            
            body = body + "<p><input type=\"submit\" value=\"Put\" /></fieldset></p>"+
                "</form>"+
                   "<form action=\""+uriInfo.getAbsolutePath().resolve(parent.getCodFiscale()).toASCIIString()+"\" method=\"DELETE\">"+
                    "<p><input type=\"submit\" value=\"Delete\" /></p></form>"+
                "</body>";

            return Response.ok().entity(header + body + footer).links(parentsL).build();

        }else{
            return Response.status(Response.Status.NOT_FOUND).entity("Parent not found").links(parentsL).build();
        }
    }
    
    
    
    @GET
    @Produces({MediaType.TEXT_HTML})
    public Response findAllParentsHTML(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        
            String body = "<body><form encode=\"application/x-www-form-urlencoded\" method=\"POST\" action=\""+uriInfo.getAbsolutePath().toASCIIString()+"\">\n" +
                "<table><thead><tr>\n" +
                "<th>ID (CF) </th>\n" +
                "<th> Name</th>\n" +
                "<th> Surname</th>\n" +
                "<th> Email</th>\n" +
                "<th> Password</th>\n" +    
                "<th> Actions</th>\n" +
                "</tr></thead><tbody>";
            
        try{
            List<Parents> listClass=super.findAll();
            for(Parents p: listClass){
                body = body + "	<tr>\n" +
                    "<td>"+p.getCodFiscale()+"</td>\n" +
                    "<td>"+p.getName()+"</td>\n" +
                    "<td>"+p.getSurname()+"</td>\n" +
                    "<td>"+p.getEmail()+"</td>\n" +
                    "<th> *****</th>\n" +
                    "<td><a href=\""+AbstractFacade.baseUri+"parents/"+p.getCodFiscale()+"\"> View and edit and delete </a>" +
                "</tr>";
            }
            body = body + "	<tr>\n" +
                    "<td><input name = \"cf\" ></td>\n" +
                    "<td><input name = \"name\"></td>\n" +
                    "<td><input name = \"surname\"></td>\n" +
                    "<td><input name = \"email\"></td>\n" +
                    "<td><input name = \"password\"></td>\n" +
                    "<td><input type=\"submit\" value=\"Add (POST)\" />" +
                "</tr>";
            body = body + "</tbody></table></body>";
           
            
            return Response.ok().entity(header + body + footer).links(parentsL).build();
        }catch(NoResultException ex){
            body = body + "</tbody></table></body>";
            return Response.ok().entity(header + body + footer).links(parentsL).build();
        }
    }
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllParents(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        try{
            List<Parents> listClass=super.findAll();
            List<Parents> copy=new ArrayList<>();
            listClass.forEach(p->copy.add(new Parents(p)));
            injectLinks(copy);
            return this.convertType(type, copy).links(adminL, parentsL).build();
        }catch(NoResultException ex){
            return Response.ok().entity("No Parent is stored in the system").links(adminL, parentsL).build();
        }
    }

    @Override
    public List<Parents> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Parents> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(parentsL).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Parents> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Parents>> generic = new GenericEntity<List<Parents>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Parents> copy){
        copy.forEach(p->p.setSelf(new LinkH(AbstractFacade.baseUri+localPath+p.getCodFiscale(), "self", "GET")));
        copy.forEach(p->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+uriInfo.getPath()+"/"+p.getCodFiscale(), "edit", "PUT and DELETE")));
        //copy.forEach(p->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+"students/", "students", "GET")));
        //copy.forEach(p->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+"notifications/", "notifications", "GET")));
        //copy.forEach(p->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+"appointments/", "appointments", "GET")));
        for(Parents p: copy){
            if(p.getStudentsChildren() != null){
                p.getStudentsChildren().forEach(s->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+"students/"+s, "student", "GET")));
            }
        }           
    }
    
    private void injectLink(Parents p){
        p.setSelf(new LinkH(baseUri+localPath+p.getCodFiscale(), "self", "GET"));
        p.addLink(new LinkH(baseUri+localPath+p.getCodFiscale(), "edit", "PUT and DELETE"));
        p.addLink(new LinkH(baseUri+"students/", "students", "GET"));
        p.addLink(new LinkH(baseUri+"notifications/", "notifications", "GET"));
        p.addLink(new LinkH(baseUri+"appointments/", "appointments", "GET"));
        p.addLink(new LinkH(baseUri+"payments/", "payments", "GET"));
        if(p.getStudentsChildren() != null){
            p.getStudentsChildren().forEach(s->p.addLink(new LinkH(uriInfo.getBaseUri().toASCIIString()+"students/"+s, "student", "GET")));
        }        
    }
}
