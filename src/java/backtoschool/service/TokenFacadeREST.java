/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Students;
import backtoschool.Token;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("token")
public class TokenFacadeREST extends AbstractFacade<Token> {

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;

    public TokenFacadeREST() {
        super(Token.class);
    }

    //@POST
    @Override
    //@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Token entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") String id, Token entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Token find(@PathParam("id") String id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Token> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Token> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    //CONTROLLARE SE VA BENE TODO
    @Override
    public Token findByParameter(String query,String par,Object value) throws NoResultException{
       try{
        String tokenValue = (String) value;
        String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        return this.getEntityManager().createNamedQuery(query, Token.class).setParameter(par,tokenValueSplitted).getSingleResult();
       }catch(NullPointerException ex){
           return null;
       }
       catch(NoResultException ex){
           return null;
       }
    }
    
    @Override
    public Response.ResponseBuilder convertType(String type, Token entity){
        switch(type){
            case "application/xml":
                GenericEntity<Token> generic = new GenericEntity<Token> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
}
