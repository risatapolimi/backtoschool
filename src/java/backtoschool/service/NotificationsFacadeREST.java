/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.Administrators;
import backtoschool.LinkH;
import backtoschool.Notifications;
import backtoschool.Token;
import static backtoschool.service.AbstractFacade.baseUri;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("notifications")
public class NotificationsFacadeREST extends AbstractFacade<Notifications> {
    
    @Context
    UriInfo uriInfo;
    
    Link notificationsL;
    Link generalL,personalL;
    Link administratorsL, teachersL, parentsL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB 
    private AdministratorsFacadeREST adminFacade;
    
    @EJB 
    private StudentsFacadeREST studentsFacade;
    
    @EJB 
    private TeachingFacadeREST teachingFacade;
    

    public NotificationsFacadeREST() {
        super(Notifications.class);
        localPath = "notifications/";
        notificationsL = Link.fromUri(baseUri+localPath).rel("notifications").title("GET").build();
        generalL = Link.fromUri(baseUri+localPath+"general").rel("general notifications").title("GET").build();  
        personalL = Link.fromUri(baseUri+localPath+"personal").rel("personal notifications").title("GET").build();
        administratorsL = Link.fromUri(baseUri+"administrators/").rel("administrators").title("GET").build(); 
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNotification(String entityNotification, @HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        return addNotification(gson.fromJson(entityNotification,Notifications.class),type,tokenValue);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addNotification(Notifications entity,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        //String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        try{
            Administrators admin=adminFacade.find(token.getCodUser());
            entity.setCodAdmin(admin);
            entity.setToParent(false);
            if(entity.getDate()==null)
                entity.setDate(new Date());
            this.create(entity);
            em.flush();
            return Response.created(URI.create(baseUri+localPath+entity.getId())).links(notificationsL).build();
        }catch(NoResultException ex1){
            return Response.status(Response.Status.NOT_FOUND).entity("Admin not found").links(notificationsL).build();
        }
    }
    
    
    @Override
    public void create(Notifications entity) {
        super.create(entity);
    }

    /*@PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Notifications entity) {
        super.edit(entity);
    }*/

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            Notifications not=super.find(id);
            if(not!=null){
                super.remove(not);
                return Response.status(Response.Status.NO_CONTENT).links(notificationsL).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity("Notification not found").links(notificationsL).build();
            }    
        }catch( NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Notification not found").links(notificationsL).build();
        }   
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") Integer id,@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        try{
            Notifications not=super.find(id);
            not.convertBroadcastTo();
            not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
            injectLinks(not, token.getUserType());
            switch(userType){
                case "admin":
                    response=super.checkPermission(tokenValue, "admin");
                    if(response!=null)
                        return response;
                    
                    return super.convertType(type, not).links(notificationsL, administratorsL).build();
                case "parent":
                    response=super.checkPermission(tokenValue, "parent");
                    if(response!=null)
                        return response;
                    if(not.getBroadcast()==6 && not.getDestination().equals(token.getCodUser())){
                        return super.convertType(type, not).links(notificationsL).build();
                    }else if(not.getBroadcast()==0 || not.getBroadcast() == 1){
                        return super.convertType(type, not).links(notificationsL).build();
                    }else if(not.getBroadcast()==3){
                        List<String> classStudents=this.studentsFacade.getEntityManager().createNamedQuery("Students.findChildrenClass", String.class)
                           .setParameter("codFiscale", token.getCodUser()).getResultList();
                        if(classStudents.contains(not.getDestination())){
                           return super.convertType(type, not).links(notificationsL).build();
                        }
                    }
                    return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to see this notification").links(notificationsL, generalL, personalL).build();
                
                case "teacher":
                    response=super.checkPermission(tokenValue, "teacher");
                    if(response!=null)
                        return response;
                    if(not.getBroadcast()==5 && not.getDestination().equals(token.getCodUser())){
                        return super.convertType(type, not).links(notificationsL).build();
                    }else if(not.getBroadcast()==0 || not.getBroadcast() == 2){
                        return super.convertType(type, not).links(notificationsL).build();
                    }else if(not.getBroadcast()==4){
                        List<String> classTeacher=this.teachingFacade.getEntityManager().createNamedQuery("Teaching.findClassByCodTeacher", String.class)
                                .setParameter("codTeacher", token.getCodUser()).getResultList();
                        if(classTeacher.contains(not.getDestination())){
                           return super.convertType(type, not).links(notificationsL).build();
                        }
                    }
                    return Response.status(Response.Status.FORBIDDEN).entity("You are not allowed to see this notification").links(notificationsL, generalL, personalL).build();

                default:
                    return super.checkPermission(tokenValue, userType);
                    
            }
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Notification not found").links(notificationsL).build();
        }
    }
    
    
    
    @GET
    @Path("personal")
    public Response findPersonalNotification(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Notifications> notifications=new ArrayList<>();
        List<Notifications> classNotifications=new ArrayList<>();
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                    notifications=super.findListByParameter("Notifications.findByDestination", "destination", token.getCodUser());
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
                
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                    notifications=super.findListByParameter("Notifications.findByAdmin", "codFiscale", token.getCodUser());
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    return this.convertType(type, notifications).links(administratorsL, notificationsL, personalL).build();
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                    notifications=super.findListByParameter("Notifications.findByDestination", "destination", token.getCodUser());
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    
    @GET
    @Path("general")
    public Response findGeneralNotification(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue){
        //String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Notifications> notifications=new ArrayList<>();
        List<Notifications> classNotifications=new ArrayList<>();;
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                    List<String> classStudents=this.studentsFacade.getEntityManager().createNamedQuery("Students.findChildrenClass", String.class)
                            .setParameter("codFiscale", token.getCodUser()).getResultList();
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 0));
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 2));
                    classNotifications=super.findListByParameter("Notifications.findByBroadcast", "broadcast", 3);
                    classNotifications.stream().filter(c->classStudents.contains(c.getDestination()));
                    notifications.addAll(classNotifications);
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                    List<String> classTeacher=this.teachingFacade.getEntityManager().createNamedQuery("Teaching.findClassByCodTeacher", String.class)
                            .setParameter("codTeacher", token.getCodUser()).getResultList();
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 0));
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 1));
                    classNotifications=super.findListByParameter("Notifications.findByBroadcast", "broadcast", 4);
                    classNotifications.stream().filter(c->classTeacher.contains(c.getDestination()));
                    notifications.addAll(classNotifications);
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
                default: 
                    return super.checkPermission(tokenValue, "teacher");
        }
    }
    
    @GET
    public Response findAllNotification(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        //String tokenValueSplitted = AbstractFacade.splitAuthToken(tokenValue);
        
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        if (token == null){
            return AbstractFacade.UnauthorizedResponse();
        }
        String userType = token.getUserType();
        Response response;
        List<Notifications> notifications=new ArrayList<>();
        List<Notifications> classNotifications=new ArrayList<>();
        switch (userType){
            case "parent":
                response=super.checkPermission(tokenValue, "parent");
                if(response!=null)
                    return response;
                    List<String> classStudents=this.studentsFacade.getEntityManager().createNamedQuery("Students.findChildrenClass", String.class)
                            .setParameter("codFiscale", token.getCodUser()).getResultList();
                    notifications=super.findListByParameter("Notifications.findByDestination", "destination", token.getCodUser());
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 0));
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 2));
                    classNotifications=super.findListByParameter("Notifications.findByBroadcast", "broadcast", 3);
                    classNotifications.stream().filter(c->classStudents.contains(c.getDestination()));
                    notifications.addAll(classNotifications);
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
                
            case "admin":
                response=super.checkPermission(tokenValue, "admin");
                if(response!=null)
                    return response;
                    notifications=this.findAll();
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(administratorsL, notificationsL, personalL).build();
            case "teacher":
                response=super.checkPermission(tokenValue, "teacher");
                if(response!=null)
                    return response;
                    List<String> classTeacher=this.teachingFacade.getEntityManager().createNamedQuery("Teaching.findClassByCodTeacher", String.class)
                            .setParameter("codTeacher", token.getCodUser()).getResultList();
                    notifications=super.findListByParameter("Notifications.findByDestination", "destination", token.getCodUser());
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 0));
                    notifications.addAll(super.findListByParameter("Notifications.findByBroadcast", "broadcast", 1));
                    classNotifications=super.findListByParameter("Notifications.findByBroadcast", "broadcast", 4);
                    classNotifications.stream().filter(c->classTeacher.contains(c.getDestination()));
                    notifications.addAll(classNotifications);
                    for(Notifications not:notifications){
                        not.convertBroadcastTo();
                        not.setStringCodAdmin(not.getCodAdmin().getCodFiscale());
                    }
                    injectLinks(notifications, token.getUserType());
                    return this.convertType(type, notifications).links(notificationsL, generalL, personalL).build();
                default: 
                    return super.checkPermission(tokenValue, "admin");
        }
    }
    

    @Override
    public List<Notifications> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Notifications> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }*/

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public Response countREST(@HeaderParam("Authorization")String tokenValue) {
        
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        return Response.ok(String.valueOf(super.count())).links(notificationsL).build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
     public Response.ResponseBuilder convertType(String type, List<Notifications> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Notifications>> generic = new GenericEntity<List<Notifications>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
     
    private void injectLinks(List<Notifications> copy, String role){
        copy.forEach(p->p.clearLinks());
        copy.forEach(p->p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "GET")));
        if("admin".equals(role)){
            copy.forEach(p->p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT and DELETE")));
            copy.forEach(p->p.addLink(new LinkH(baseUri+"administrators/"+p.getStringCodAdmin(), "creator", "GET")));
        }
        
    }
    
    private void injectLinks(Notifications p, String role){
        p.clearLinks();
        p.setSelf(new LinkH(baseUri+localPath+p.getId(), "self", "GET"));
        p.addLink(new LinkH(baseUri+localPath, "notifications", "GET"));
        if("admin".equals(role)){
            p.addLink(new LinkH(baseUri+"administrators/"+p.getStringCodAdmin(), "creator", "GET"));
            p.addLink(new LinkH(baseUri+localPath+p.getId(), "edit", "PUT and DELETE"));
            p.addLink(new LinkH(baseUri+localPath+"personal", "personal notifications", "GET"));
        }else{
            p.addLink(new LinkH(baseUri+localPath+"general", "general notifications", "GET"));  
            
        }
        
        
    }
        
}
