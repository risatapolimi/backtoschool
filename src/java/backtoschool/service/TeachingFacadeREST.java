/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import backtoschool.LinkH;
import backtoschool.Notifications;
import backtoschool.Teachers;
import backtoschool.Teaching;
import backtoschool.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.URI;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("teachings")
public class TeachingFacadeREST extends AbstractFacade<Teaching> {
    
    @Context
    UriInfo uriInfo;
    
    Link teachingsL, adminL;

    @PersistenceContext(unitName = "BackToSchoolPU")
    private EntityManager em;
    
    @EJB
    TeachersFacadeREST teachersFacadeREST;

    public TeachingFacadeREST() {
        super(Teaching.class);
        localPath = "teachings/";
        teachingsL = Link.fromUri(baseUri+localPath).rel("self").title("GET").build();
        adminL = Link.fromUri(baseUri+"administrators/").rel("administrators").title("GET, POST").build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response create(Teaching entity, @HeaderParam("Authorization")String tokenValue) {
         Response response=super.checkPermission(tokenValue, "admin");
        
        if(response!=null)
            return response;
        Teachers teacher = teachersFacadeREST.findByParameter("Teachers.findByCodFiscale","codTeacher",entity.getStringCodeTeacher());
        if(teacher != null){
            entity.setCodTeacher(teacher);
            super.create(entity); 
            em.flush();
            return Response.created(URI.create(baseUri+localPath+entity.getId())).links(teachingsL).build();  
        }
        else{
            return Response.status(Response.Status.CONFLICT).entity("Teacher does not exist, please insert him before").links(teachingsL).build();
        } 
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("id") Integer id, Teaching entity, @HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            Teaching teaching=super.findByParameter("Teaching.findById","id",id);
            if(teaching != null){
                try{
                    Teachers teacher;
                    if(entity.getStringCodeTeacher()==null || entity.getStringCodeTeacher().isEmpty())
                        teacher=teaching.getCodTeacher();
                    else teacher= teachersFacadeREST.findByParameter("Teachers.findByCodFiscale","codFiscale",entity.getStringCodeTeacher());
                    if(teacher != null){
                        entity.setCodTeacher(teacher);
                        try{
                            if(entity.getClass1()==null || entity.getClass1().isEmpty())
                                entity.setClass1(teaching.getClass1());
                            if(entity.getArgument()==null || entity.getArgument().isEmpty())
                                entity.setArgument(teaching.getArgument());
                            super.edit(entity);
                            return Response.ok().build();
                        }catch(Exception e){
                            return Response.status(Response.Status.BAD_REQUEST).entity("Please check inserted data").build(); //TODO links
                        }
                    }else{
                        return Response.status(Response.Status.CONFLICT).entity("Teacher does not exist, please insert him before").links(teachingsL).build();
                    }
                }catch(NullPointerException | NoResultException ex){
                    return Response.status(Response.Status.CONFLICT).entity("Teacher does not exist, please insert him before").links(teachingsL).build();
                }
            }
            return Response.status(Response.Status.NOT_FOUND).links().build();
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).links().build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Integer id, @HeaderParam("Authorization")String tokenValue) {
          Response response=super.checkPermission(tokenValue, "admin");
        if(response!=null)
            return response;
        try{
            Teaching teaching=super.find(id);
            if(teaching==null)
                return Response.status(Response.Status.NOT_FOUND).entity("Teaching not found").build();
            super.remove(teaching);
            return Response.status(Response.Status.NO_CONTENT).build();
        }catch(NullPointerException | NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("Teaching not found").build();
        } 
    }

    @GET //tutte le classi in cui insegna un docente
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public Response find(@HeaderParam("Accept") String type,@HeaderParam("Authorization")String tokenValue) {
        Response response=super.checkPermission(tokenValue, "teacher");
        
        if(response!=null){
            response=super.checkPermission(tokenValue, "admin");
            if (response!=null)
                return response;
        }
        //String tokenValue = AbstractFacade.splitAuthToken(tokenValue);
        Token token=this.tokenFacade.findByParameter("Token.findByValue", "value", tokenValue);
        String role = token.getUserType();
        switch(role){
            case "teacher":
        try{
            List<Teaching> teachingList=super.findListByParameter("Teaching.findByCodTeacher","codTeacher",token.getCodUser());
            teachingList.forEach((teaching) -> teaching.setStringCodeTeacher(teaching.getCodTeacher().getCodFiscale()));
            injectLinks(teachingList, token.getUserType());
            return this.convertType(type, teachingList).links(teachingsL).build();
        }
        catch(NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("No teaching was found").links(teachingsL).build();
        }
            case "admin":
                try{
            List<Teaching> teachingList=super.findAll();
            teachingList.forEach((teaching) -> teaching.setStringCodeTeacher(teaching.getCodTeacher().getCodFiscale()));
            injectLinks(teachingList, token.getUserType());
            return this.convertType(type, teachingList).links(adminL, teachingsL).build();
        }
        catch(NoResultException ex){
            return Response.status(Response.Status.NOT_FOUND).entity("No teaching was found").links(adminL, teachingsL).build();
        }
            default:
                return Response.status(Response.Status.FORBIDDEN).links().build();
        }
    }
    

    @Override
    public List<Teaching> findAll() {
        return super.findAll();
    }

    /*@GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Teaching> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }*/

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
    public Response.ResponseBuilder convertType(String type, List<Teaching> entity){
        switch(type){
            case "application/xml":
                GenericEntity<List<Teaching>> generic = new GenericEntity<List<Teaching>> (entity){};
                return Response.ok(generic,MediaType.APPLICATION_XML);
            case "application/json":
                return Response.ok(gson.toJson(entity),MediaType.APPLICATION_JSON);
            case "text/plain":
                return Response.ok(entity.toString(),MediaType.TEXT_PLAIN);
            default:
                return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Media type not generated");
        }
    }
    
    private void injectLinks(List<Teaching> copy, String role){
        copy.forEach(t->t.clearLinks());    
        copy.forEach(t->t.setSelf(new LinkH(baseUri+"teachings/"+t.getId(), "self", "---")));
        if("admin".equals(role)){
            copy.forEach(t->t.setSelf(new LinkH(baseUri+"teachings/"+t.getId(), "edit", "PUT")));
        }
        copy.forEach(t->t.addLink(new LinkH(baseUri+"students/class/"+t.getClass1(), "class", "GET")));    
               
    }
    
}
