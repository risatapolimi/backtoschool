/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Daniele
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(backtoschool.service.AdministratorsFacadeREST.class);
        resources.add(backtoschool.service.AppointmentsFacadeREST.class);
        resources.add(backtoschool.service.GradesFacadeREST.class);
        resources.add(backtoschool.service.LessonTimetableFacadeREST.class);
        resources.add(backtoschool.service.NotificationsFacadeREST.class);
        resources.add(backtoschool.service.ParentsFacadeREST.class);
        resources.add(backtoschool.service.PaymentsFacadeREST.class);
        resources.add(backtoschool.service.StudentsFacadeREST.class);
        resources.add(backtoschool.service.TeachersFacadeREST.class);
        resources.add(backtoschool.service.TeachingFacadeREST.class);
        resources.add(backtoschool.service.TokenFacadeREST.class);
    }
    
}
