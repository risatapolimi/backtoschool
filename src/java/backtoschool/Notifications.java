/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backtoschool;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "notifications")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notifications.findAll", query = "SELECT n FROM Notifications n")
    , @NamedQuery(name = "Notifications.findById", query = "SELECT n FROM Notifications n WHERE n.id = :id")
    , @NamedQuery(name = "Notifications.findByNotice", query = "SELECT n FROM Notifications n WHERE n.notice = :notice")
    , @NamedQuery(name = "Notifications.findByToParent", query = "SELECT n FROM Notifications n WHERE n.toParent = :toParent")
    , @NamedQuery(name = "Notifications.findByBroadcast", query = "SELECT n FROM Notifications n WHERE n.broadcast = :broadcast")
    , @NamedQuery(name = "Notifications.findByDestination", query = "SELECT n FROM Notifications n WHERE n.destination = :destination")
    , @NamedQuery(name = "Notifications.findByAdmin", query = "SELECT n FROM Notifications n WHERE n.codAdmin.codFiscale = :codFiscale")
    , @NamedQuery(name = "Notifications.findByDate", query = "SELECT n FROM Notifications n WHERE n.date = :date")})
public class Notifications extends ResourceHATEOAS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @XmlTransient
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "notice")
    @Expose
    private String notice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "toParent")
    @XmlTransient
    private boolean toParent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "broadcast")
    @Expose
    private int broadcast; // 0: per tutti, 1: per tutti i teachers, 2: per tutti i parents, 3: per i parents di una class, 4: per i teachers di una classe
    //5: per un teacher personale, 6:per un parent personale
    @Size(max = 30)
    @Column(name = "destination")
    @Expose
    private String destination;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    @Expose
    private Date date;
    @JoinColumn(name = "cod_admin", referencedColumnName = "cod_fiscale")
    @ManyToOne(optional = false)
    @XmlTransient
    private Administrators codAdmin;
    
    @Transient
    @Expose
    private String stringCodAdmin;
    
    @Transient
    @Expose
    private String to;

    public Notifications() {
    }

    public Notifications(Integer id) {
        this.id = id;
    }

    public Notifications(Integer id, String notice, boolean toParent, int broadcast, Date date) {
        this.id = id;
        this.notice = notice;
        this.toParent = toParent;
        this.broadcast = broadcast;
        this.date = date;
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    @XmlTransient
    public boolean getToParent() {
        return toParent;
    }

    public void setToParent(boolean toParent) {
        this.toParent = toParent;
    }

    public int getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(int broadcast) {
        this.broadcast = broadcast;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @XmlTransient
    public Administrators getCodAdmin() {
        return codAdmin;
    }

    public void setCodAdmin(Administrators codAdmin) {
        this.codAdmin = codAdmin;
    }

    public String getStringCodAdmin() {
        return stringCodAdmin;
    }

    public void setStringCodAdmin(String stringCodAdmin) {
        this.stringCodAdmin = stringCodAdmin;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
    
    public void convertBroadcastTo(){
        switch(broadcast){
            case 0:
                this.to="AllPeople";
            break;
            case 1:
                this.to="AllTeachers";
            break;
            case 2:
                this.to="AllParents";
            break;
            case 3:
                this.to="ParentsOfClass";
            break;
            case 4:
                this.to="TeacherOfClass";
            break;
            case 5:
                this.to="Personal";
            break;
            case 6:
                this.to="Personal";
            break;
        }
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notifications)) {
            return false;
        }
        Notifications other = (Notifications) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "backtoschool.Notifications[ id=" + id + " ]";
    }
    
}
