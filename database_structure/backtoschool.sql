-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: localhost    Database: backtoschool
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `backtoschool`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `backtoschool` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `backtoschool`;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `cod_fiscale` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_fiscale`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
INSERT INTO `administrators` VALUES ('Daniele','Uboldi','dnlubd020595e506t','1995-05-02','uboldi.daniele@gmail.com','funkoforever'),('Daniele','Riva','rvidnl94p0e507','1994-09-05','danix094@gmail.com','cicciodani94'),('Marco','Sartini','srtmrc040294e507p','1994-02-04','marco.sartini.ms@gmail.com','vivaleradio'),('Giorgio','Tavecchia','tvcgrg030594e506t','1994-05-03','tavecchia.giorgio@gmail.com','pinguinibelli');
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `cod_teacher` varchar(30) NOT NULL,
  `cod_parent` varchar(30) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `accepted` bit(1) NOT NULL,
  `last_editor` bit(1) NOT NULL,
  `note` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cod_teacher` (`cod_teacher`),
  KEY `cod_parent` (`cod_parent`),
  CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`cod_teacher`) REFERENCES `teachers` (`cod_fiscale`),
  CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`cod_parent`) REFERENCES `parents` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES ('stfznr010976e302f','srglvl130958e407g',3,'2018-10-12',_binary '',_binary '\0','Richiedi colloquio per studente non disciplinato , ok va bene');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grades` (
  `cod_student` varchar(30) NOT NULL,
  `cod_teacher` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `mark` double NOT NULL DEFAULT '6',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `argument` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cod_student` (`cod_student`),
  KEY `cod_teacher` (`cod_teacher`),
  CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`cod_student`) REFERENCES `students` (`cod_fiscale`),
  CONSTRAINT `grades_ibfk_2` FOREIGN KEY (`cod_teacher`) REFERENCES `teachers` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` VALUES ('mrclvl220902e512e','stfznr010976e302f','2018-09-25',8,1,'Informatica'),('rcrlvl170701e497r','stfznr010976e302f','2018-09-25',5,2,'Sicurezza');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson_timetable`
--

DROP TABLE IF EXISTS `lesson_timetable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lesson_timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(20) NOT NULL,
  `argument` varchar(20) NOT NULL,
  `cod_teacher` varchar(30) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `day` enum('monday','tuesday','wednesday','thursday','friday','saturday','sunday') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cod_teacher` (`cod_teacher`),
  CONSTRAINT `lesson_timetable_ibfk_1` FOREIGN KEY (`cod_teacher`) REFERENCES `teachers` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson_timetable`
--

LOCK TABLES `lesson_timetable` WRITE;
/*!40000 ALTER TABLE `lesson_timetable` DISABLE KEYS */;
INSERT INTO `lesson_timetable` VALUES (1,'1E','Sicurezza','stfznr010976e302f','09:05:00','10:00:00','monday');
/*!40000 ALTER TABLE `lesson_timetable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notice` varchar(250) NOT NULL,
  `toParent` bit(1) NOT NULL,
  `broadcast` int(11) NOT NULL,
  `destination` varchar(30) DEFAULT NULL,
  `cod_admin` varchar(30) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cod_admin` (`cod_admin`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`cod_admin`) REFERENCES `administrators` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'Domani non ci sar?á lezione',_binary '\0',0,NULL,'rvidnl94p0e507','2018-09-26'),(3,'Oggi ?¿ esploso il laboratorio di chimica',_binary '',6,'srglvl130958e407g','rvidnl94p0e507','2018-09-26'),(4,'Oggi ?¿ esploso il laboratorio di chimica',_binary '\0',5,'stfznr010976e302f','rvidnl94p0e507','2018-09-26'),(5,'Nota di classe',_binary '',3,'1E','rvidnl94p0e507','2018-09-26');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parents`
--

DROP TABLE IF EXISTS `parents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parents` (
  `cod_fiscale` varchar(30) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_fiscale`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parents`
--

LOCK TABLES `parents` WRITE;
/*!40000 ALTER TABLE `parents` DISABLE KEYS */;
INSERT INTO `parents` VALUES ('srglvl130958e407g','Sergio','Lavelli','sergio.lavelli@gmail.com','fozainter');
/*!40000 ALTER TABLE `parents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_parent` varchar(30) NOT NULL,
  `cod_student` varchar(30) NOT NULL,
  `causal` varchar(100) NOT NULL,
  `issuing_date` date NOT NULL,
  `payment_date` date DEFAULT NULL,
  `isPayed` bit(1) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cod_parent` (`cod_parent`),
  KEY `cod_student` (`cod_student`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`cod_parent`) REFERENCES `parents` (`cod_fiscale`),
  CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`cod_student`) REFERENCES `students` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'srglvl130958e407g','mrclvl220902e512e','monthly payment','2018-09-26','2018-09-26',_binary '',10.12),(2,'srglvl130958e407g','rcrlvl170701e497r','payment material','2018-09-26',NULL,_binary '\0',20.25),(3,'srglvl130958e407g','rcrlvl170701e497r','payment trip Barcellona','2018-09-26',NULL,_binary '\0',30.45),(4,'srglvl130958e407g','rcrlvl170701e497r','payment trip Milano','2018-09-26','2018-09-26',_binary '',25.45),(5,'srglvl130958e407g','rcrlvl170701e497r','payment trip Monza','2018-09-26',NULL,_binary '\0',8.45),(6,'srglvl130958e407g','rcrlvl170701e497r','payment material for science','2018-09-28',NULL,_binary '\0',100.25);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `class` varchar(10) DEFAULT NULL,
  `cod_fiscale` varchar(30) NOT NULL,
  `cod_fisc_parent` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_fiscale`),
  KEY `cod_fisc_parent` (`cod_fisc_parent`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`cod_fisc_parent`) REFERENCES `parents` (`cod_fiscale`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES ('Marco','Lavelli','2002-09-02','1E','mrclvl220902e512e','srglvl130958e407g'),('Riccardo','Lavelli','2001-07-17','2A','rcrlvl170701e497r','srglvl130958e407g');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `cod_fiscale` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`cod_fiscale`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES ('Stefano','Zanero','stfznr010976e302f','stefano.zanero@gmail.com','hackArcobaleni');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaching`
--

DROP TABLE IF EXISTS `teaching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaching` (
  `cod_teacher` varchar(30) NOT NULL,
  `class` varchar(20) NOT NULL,
  `argument` varchar(30) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `cod_teacher` (`cod_teacher`),
  CONSTRAINT `teaching_ibfk_1` FOREIGN KEY (`cod_teacher`) REFERENCES `teachers` (`cod_fiscale`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaching`
--

LOCK TABLES `teaching` WRITE;
/*!40000 ALTER TABLE `teaching` DISABLE KEYS */;
INSERT INTO `teaching` VALUES ('stfznr010976e302f','1E','Sicurezza',1),('stfznr010976e302f','1E','Informatica e Diritto',2),('stfznr010976e302f','2H','Informatica',3);
/*!40000 ALTER TABLE `teaching` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `cod_user` varchar(30) NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_type` varchar(10) NOT NULL,
  PRIMARY KEY (`cod_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES ('rvidnl94p0e507','ZGFuaXgwOTRAZ21haWwuY29tOmNpY2Npb2Rhbmk5NA==','2018-10-02 07:59:34','admin'),('srglvl130958e407g','c2VyZ2lvLmxhdmVsbGlAZ21haWwuY29tOmZvemFpbnRlcg==','2018-10-02 07:59:34','parent'),('stfznr010976e302f','c3RlZmFuby56YW5lcm9AZ21haWwuY29tOmhhY2tBcmNvYmFsZW5p','2018-10-02 07:58:44','teacher');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-06 13:10:17
